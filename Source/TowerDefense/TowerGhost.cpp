// Fill out your copyright notice in the Description page of Project Settings.


#include "TowerGhost.h"
#include "TowerData.h"
#include "Tower.h"
#include "BuildableNode.h"
#include "GameFramework/PlayerController.h"
#include "Components/BoxComponent.h"
#include "Components/StaticMeshComponent.h"
#include "Components/SphereComponent.h"
#include "Components/SceneComponent.h"

// Sets default values
ATowerGhost::ATowerGhost()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	SceneComponent = CreateDefaultSubobject<USceneComponent>(TEXT("Root Scene Component"));
	RootComponent = SceneComponent;

	BaseStaticMesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("Base Mesh Component"));
	BaseStaticMesh->SetupAttachment(RootComponent);

	HeadStaticMesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("Head Mesh Component"));
	HeadStaticMesh->SetupAttachment(BaseStaticMesh);

	SphereComponent = CreateDefaultSubobject<USphereComponent>(TEXT("Sphere Component"));
	SphereComponent->SetupAttachment(RootComponent);

	BoxComponent = CreateDefaultSubobject<UBoxComponent>(TEXT("Collision Box Component"));
	BoxComponent->SetupAttachment(RootComponent);
}

// Called when the game starts or when spawned
void ATowerGhost::BeginPlay()
{
	Super::BeginPlay();
	
	// Listen
	BoxComponent->OnComponentBeginOverlap.AddDynamic(this, &ATowerGhost::OnActorOverlap);
	BoxComponent->OnComponentEndOverlap.AddDynamic(this, &ATowerGhost::OnActorExitOverlap);

	// Upon starting make the mesh material red to indicate the tower can't be placed there 
	BaseStaticMesh->SetMaterial(0, unableToPlaceMat);
	HeadStaticMesh->SetMaterial(0, unableToPlaceMat);

	// Disable until activated by Build Manager
	DisableGhostTower();
}

void ATowerGhost::OnActorOverlap(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult)
{
	// checks if the other actor is a buildable node (the collision box in buildable node to be exact) 
	if (ABuildableNode* target = Cast<ABuildableNode>(OtherActor))
	{
		// Check again, make sure that the node doesn't contain an existing tower
		if (target->currentTower == NULL)
		{
			// if yes, ten change the material to green to indicate that tower can be placed
			BaseStaticMesh->SetMaterial(0, canPlaceMat);
			HeadStaticMesh->SetMaterial(0, canPlaceMat);

			// also assign the connectedNode for reference 
			currentNodeHovered = target;

			canBuildInLocation = true;
		}
	}
}

void ATowerGhost::OnActorExitOverlap(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex)
{
	// check if the exited actor was the latest referenced buildable node. If yes, then change mateiral to red again 
	if (Cast<ABuildableNode>(OtherActor) == currentNodeHovered)
	{
		BaseStaticMesh->SetMaterial(0, unableToPlaceMat);
		HeadStaticMesh->SetMaterial(0, unableToPlaceMat);
		currentNodeHovered = NULL;
		canBuildInLocation = false;
	}
}

void ATowerGhost::DisableGhostTower()
{
	// hide meshes 
	BaseStaticMesh->SetVisibility(false, true); //2nd parameter is if apply to children
	//HeadStaticMesh->ToggleVisibility(false); // used toggle at first no wonder it wasn't working as intended

	// hide sphere component
	SphereComponent->bHiddenInGame = true;
	SphereComponent->SetVisibility(false);
}

void ATowerGhost::SetTowerGhostData(class UTowerData* newTowerData)
{
	BaseStaticMesh->SetStaticMesh(newTowerData->Tower->GetDefaultObject<ATower>()->BaseStaticMesh->GetStaticMesh()); // might want to double check if the mesh also becomes null or if it will error and crash 
	HeadStaticMesh->SetStaticMesh(newTowerData->Tower->GetDefaultObject<ATower>()->HeadStaticMesh->GetStaticMesh()); // the current static mesh becoming null is the desired behavior (if there is no "Head comoponent")

	// Position / Location of the headstatic mesh if applicable. didn't modify the base static mesh translate and rotation since it's the root component (might be a bad idea actually to have it as root)
	HeadStaticMesh->SetRelativeTransform(newTowerData->Tower->GetDefaultObject<ATower>()->HeadStaticMesh->GetRelativeTransform());
	BaseStaticMesh->SetRelativeTransform(newTowerData->Tower->GetDefaultObject<ATower>()->BaseStaticMesh->GetRelativeTransform());

	// sphere size then show it
	SphereComponent->SetSphereRadius(newTowerData->Tower->GetDefaultObject<ATower>()->SphereComponent->GetUnscaledSphereRadius()); //other available option is scaled sphere radius. 
    SphereComponent->bHiddenInGame = false;
	SphereComponent->SetVisibility(true);

	// Scene node scaling copy
	SceneComponent->SetRelativeTransform(newTowerData->Tower->GetDefaultObject<ATower>()->SceneComponent->GetRelativeTransform());

	// Show meshes
	BaseStaticMesh->SetVisibility(true, true);
	// HeadStaticMesh->ToggleVisibility(true);

	// Materials
	BaseStaticMesh->SetMaterial(0, unableToPlaceMat);
	HeadStaticMesh->SetMaterial(0, unableToPlaceMat);
}

// Called every frame
void ATowerGhost::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
}

