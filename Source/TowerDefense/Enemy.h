// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "MyEnums.h"
#include "GameFramework/Character.h"
#include "Enemy.generated.h"

DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FEnemyDeathSignature, int32, killReward);

UCLASS()
class TOWERDEFENSE_API AEnemy : public ACharacter
{
	GENERATED_BODY()

public:
	// Sets default values for this character's properties
	AEnemy();

	UPROPERTY(BlueprintAssignable)
		FEnemyDeathSignature OnEnemyDeath;

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override; 



	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = "Components")
		class UBoxComponent* CollisionBoxComponent;

	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = "Components")
		class UHealthComponent* EnemyHealthComponent;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
		int32 waypointIndex;

	UPROPERTY(EditAnywhere, BlueprintReadWrite) // damage they will deal to the core 
		int32 damage;

	UPROPERTY(EditAnywhere, BlueprintReadWrite) // enum type
		TEnumAsByte<EType> EnemyType;

	UFUNCTION()
		void OnHealthComponentDeath();

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	// Called to bind functionality to input
	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Movement Targets") 
		TArray<AActor*>Waypoints;

	UFUNCTION(BlueprintCallable)
		UHealthComponent* GetHealthComponent();

	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = "Components")
		class UStaticMeshComponent* StaticMesh; // referenced by the SingleTargetTower::ROtate

	UPROPERTY(VisibleAnywhere, Category = "Components")
		class UBuffManagerComponent* BuffManagerComponent;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		int32 killReward;

	UPROPERTY(VisibleAnywhere, Category = "Setup")
		float speed;

	UPROPERTY(EditAnywhere, Category = "Setup")
		float baseSpeed;

	UFUNCTION()
		void ModifyHealth(float modValue);

	UFUNCTION()
		void ModifyKillReward(float modValue);

	UFUNCTION()
		void MoveToNextWaypoint();

	UFUNCTION()
		void Die();

	UFUNCTION()
		void DestroyEnemy();

	UFUNCTION()
		int32 GetDamage();

	UFUNCTION()
		void ModifySpeed(float newSpeed);

	UFUNCTION()
		TEnumAsByte<EType> GetEnemyType();
};
