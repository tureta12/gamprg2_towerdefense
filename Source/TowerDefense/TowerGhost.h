// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "TowerGhost.generated.h"

UCLASS()
class TOWERDEFENSE_API ATowerGhost : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	ATowerGhost();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

private:
	UPROPERTY(VisibleAnywhere, Category = "Components")
		class USceneComponent* SceneComponent;

	UPROPERTY(VisibleAnywhere, Category = "Components")
		class UStaticMeshComponent* HeadStaticMesh;

	UPROPERTY(VisibleAnywhere, Category = "Components")
		class UStaticMeshComponent* BaseStaticMesh;

	UPROPERTY(VisibleAnywhere, Category = "Components")
		class USphereComponent* SphereComponent; // TO DISPLAY THE "RANGE" of the ghost tower

	UPROPERTY(VisibleAnywhere, Category = "Components")
		class UBoxComponent* BoxComponent; // To check the ground, to be used to detect a Buildable Node 

	UPROPERTY(EditAnywhere, Category = "Material")
		UMaterial* unableToPlaceMat;

	UPROPERTY(EditAnywhere, Category = "Material")
		UMaterial* canPlaceMat;

	UFUNCTION()
		void OnActorOverlap(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp,
		int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult);

	UFUNCTION()
		void OnActorExitOverlap(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp,
			int32 OtherBodyIndex);

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	UPROPERTY(VisibleAnywhere)
		class UTowerData* towerData;

	UPROPERTY(VisibleAnywhere, BlueprintReadWrite)
		bool canBuildInLocation;

	UPROPERTY(VisibleAnywhere)
		class ABuildableNode* currentNodeHovered;

	UFUNCTION()
		void SetTowerGhostData(class UTowerData* newTowerData);  // to be called by the building manager. The init basically 

	UFUNCTION()
		void DisableGhostTower();

};
