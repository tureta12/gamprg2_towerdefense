// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Projectile.h"
#include "MissleProjectile.generated.h"

/**
 * 
 */
UCLASS()
class TOWERDEFENSE_API AMissleProjectile : public AProjectile
{
	GENERATED_BODY()
	
public:
	// Sets default values for this actor's properties
	AMissleProjectile();

private:
	void BeginPlay() override;

	// array of enemies where the sphere collider will add stuff
	UPROPERTY(VisibleANywhere)
		TArray <class AEnemy*> targetsInAOERange;

	UFUNCTION()
	void OnActorOverlap(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp,
		int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult);

	UFUNCTION()
		virtual void OnActorExitOverlap(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp,
			int32 OtherBodyIndex);

	void OnProjectileOverlapEffect(AActor* otherActor) override;

public:
	UFUNCTION(BlueprintImplementableEvent)
		void MissleExplosion();

	UPROPERTY(VisibleAnywhere, Category = "Components")
		class USphereComponent* SphereComponent;
};
