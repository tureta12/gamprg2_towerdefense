// Fill out your copyright notice in the Description page of Project Settings.

#include "MissleProjectile.h"
#include "Enemy.h"
#include "HealthComponent.h"
#include "Components/SphereComponent.h"

AMissleProjectile::AMissleProjectile()
{
	SphereComponent = CreateDefaultSubobject<USphereComponent>(TEXT("Sphere Component"));
	SphereComponent->SetupAttachment(RootComponent);
}

void AMissleProjectile::BeginPlay()
{
	Super::BeginPlay();

	SphereComponent->OnComponentBeginOverlap.AddDynamic(this, &AMissleProjectile::OnActorOverlap);
	SphereComponent->OnComponentEndOverlap.AddDynamic(this, &AMissleProjectile::OnActorExitOverlap);
}

void AMissleProjectile::OnActorOverlap(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult)
{
	if (AEnemy* target = Cast<AEnemy>(OtherActor))
	{
		// add enemies if they enter the range of the sphere collider
		if (!targetsInAOERange.Contains(target))
		{
			targetsInAOERange.Add(target);
		}
	}
}

void AMissleProjectile::OnActorExitOverlap(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex)
{
	if (AEnemy* target = Cast<AEnemy>(OtherActor))
	{
		// Remove enemies if they exit ouf of the range of the sphere collider
		if (targetsInAOERange.Contains(target))
		{
			targetsInAOERange.Remove(target);
		}
	}
}

void AMissleProjectile::OnProjectileOverlapEffect(AActor* otherActor)
{
	// Go through each enemy in targestInAOERange and apply the damage to them
	for (int i = 0; i < targetsInAOERange.Num(); i++)
	{
		targetsInAOERange[i]->GetHealthComponent()->TakeDamage(damage);
	}

	// Effects
	MissleExplosion();
}

