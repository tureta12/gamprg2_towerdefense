// Fill out your copyright notice in the Description page of Project Settings.


#include "SpawnPoint.h"
#include "WaveData.h"
#include "Enemy.h"
#include "Components/ArrowComponent.h"
#include "Math/TransformNonVectorized.h"

// Sets default values
ASpawnPoint::ASpawnPoint()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	// Init
	enemySpawnDataIndex = 0;
	indexOfSimilarSpawnerName = 0;
	noEnemiesInSpawnData = 0;
	spawnedEnemiesInSpawnData = 0;
	delayBetweenSpawns = 5; // 5 seconds
	readyForNextSubwave = false;

	ArrowComponent = CreateDefaultSubobject<UArrowComponent>("Arrow Component");
	ArrowComponent->SetupAttachment(RootComponent);

}

// Called when the game starts or when spawned
void ASpawnPoint::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void ASpawnPoint::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

void ASpawnPoint::StartWave()
{
	UE_LOG(LogTemp, Warning, TEXT("StartWave"));

	noEnemiesInSpawnData = 0; // reset tracking variables
	spawnedEnemiesInSpawnData = 0;
	indexOfSimilarSpawnerName = 0;
	enemySpawnDataIndex = 0;
	readyForNextSubwave = false;

	// Parameters: TimerHandle, this, &Method, DelayBetweenLoops, IsLooping, firstDelayInSeconds
	// only way to check if it's still looping is by: GetWorld()->GetTimerManager().IsTimerActive(TimeHandle) or maybe GetWorldTimerManager()

	ExecuteSubwave();
}

void ASpawnPoint::StartWaveAfterDelay()
{
	UE_LOG(LogTemp, Warning, TEXT("Starting the wave after some delay"));
	GetWorldTimerManager().SetTimer(DelayBetweenWavesTimerHandle, this, &ASpawnPoint::StartWave, delayBetweenWaves, false); // not looping
}

void ASpawnPoint::ExecuteSubwave()
{
	// The INITIALIZATION OF VARIABLES

	UE_LOG(LogTemp, Warning, TEXT("ExecuteSubWave"));

	if (enemySpawnDataIndex >= currentWaveData->EnemySpawnData.Num())
	{
		// end the timer? since we've reached the end of the eney spawn data array
		UE_LOG(LogTemp, Warning, TEXT("timer function ended, reached end of currentWaveData array"));
		GetWorldTimerManager().ClearTimer(DelayBetweenSpawnsTimerHandle);
		return;
	}

	// otherwise, continue with the subwave
	// find out at which index of the spawners is this spawner similar to, if there is a similar one.  
	for (int i = 0; i < currentWaveData->EnemySpawnData[enemySpawnDataIndex].Spawners.Num(); i++)
	{
		if (currentWaveData->EnemySpawnData[enemySpawnDataIndex].Spawners[i]->GetDefaultObject<ASpawnPoint>()->spawnPointName == spawnPointName)
		{
			// also identify the number of enemies to be spawned  
			noEnemiesInSpawnData = currentWaveData->EnemySpawnData[enemySpawnDataIndex].numberOfEnemies;
			indexOfSimilarSpawnerName = i;
		}
	}

	GetWorldTimerManager().SetTimer(DelayBetweenSpawnsTimerHandle, this, &ASpawnPoint::HandleSpawning, delayBetweenSpawns, true, delayBetweenSubwaves); // looping
}

void ASpawnPoint::HandleSpawning()
{
	if (spawnedEnemiesInSpawnData < noEnemiesInSpawnData)
	{
		// UE_LOG(LogTemp, Warning, TEXT("spawnEnemiesInSpawnData < noEnemiesInsSpawnData"));
	    if (currentWaveData->EnemySpawnData[enemySpawnDataIndex].Spawners[indexOfSimilarSpawnerName]->GetDefaultObject<ASpawnPoint>()->spawnPointName == spawnPointName)
		{
			// Spawn stuff
			FActorSpawnParameters spawnParams;
			spawnParams.SpawnCollisionHandlingOverride = ESpawnActorCollisionHandlingMethod::AlwaysSpawn;
			spawnParams.bNoFail = true;
			spawnParams.Owner = this;

			// LatestSpawnedTile = GetWorld()->SpawnActor<ATile>(TileClass, TileSpawnLocation, spawnParams);
			if (currentWaveData->EnemySpawnData[enemySpawnDataIndex].Enemy != NULL)
			{
				// UE_LOG(LogTemp, Warning, TEXT(" it's not NULLLLLL!"));
				latestSpawnedEnemy = GetWorld()->SpawnActor<ACharacter>(currentWaveData->EnemySpawnData[enemySpawnDataIndex].Enemy, GetActorTransform()/*ArrowComponent->GetComponentTransform()*/, spawnParams);
			}

			OnEnemySpawn.Broadcast(Cast<AEnemy>(latestSpawnedEnemy));

			// SETUP SPAWNED ENEMY
			// give Waypoints
			Cast<AEnemy>(latestSpawnedEnemy)->Waypoints = Waypoints;

			// enemy health multiplier
			Cast<AEnemy>(latestSpawnedEnemy)->ModifyHealth(currentWaveData->enemyHealthMultiplier);

			// kill reward multiplier
			Cast<AEnemy>(latestSpawnedEnemy)->ModifyKillReward(currentWaveData->killRewardMultiplier);

			spawnedEnemiesInSpawnData++;

			// Make that enemy start to move 
			Cast<AEnemy>(latestSpawnedEnemy)->MoveToNextWaypoint();

			UE_LOG(LogTemp, Warning, TEXT("Spawned an enemy!"));
		}
	}
	else
	{
		GetWorldTimerManager().ClearTimer(DelayBetweenSpawnsTimerHandle);
		UE_LOG(LogTemp, Warning, TEXT("cleared the timer as this function has no enemies to spawn. Now ready for next subwave "));

		readyForNextSubwave = true;
		OnReadyForNextSubwave.Broadcast();

	}
}

FString ASpawnPoint::GetName()
{
	return spawnPointName;
}

bool ASpawnPoint::IsReadyForNextSubwave()
{
	return readyForNextSubwave;
}

void ASpawnPoint::StartNextSubwave()
{
	noEnemiesInSpawnData = 0; // reset tracking variables
	spawnedEnemiesInSpawnData = 0;
	indexOfSimilarSpawnerName = 0;
	readyForNextSubwave = false;

	enemySpawnDataIndex++;

	ExecuteSubwave();

	UE_LOG(LogTemp, Warning, TEXT("Spawner proceeds to next subwave"));
}
