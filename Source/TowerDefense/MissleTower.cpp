// Fill out your copyright notice in the Description page of Project Settings.


#include "MissleTower.h"
#include "Projectile.h"
#include "MissleProjectile.h"
#include "Components/SphereComponent.h"

void AMissleTower::OnUpgrade()
{
	Super::OnUpgrade();

}

void AMissleTower::InitSpawnedProjectile(AProjectile* spawnedProjectile)
{
	Super::InitSpawnedProjectile(spawnedProjectile);

	Cast<AMissleProjectile>(spawnedProjectile)->SphereComponent->SetSphereRadius(SphereComponent->GetUnscaledSphereRadius() * MissleSplashRadiusMulti[level]);
}
