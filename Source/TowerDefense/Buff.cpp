// Fill out your copyright notice in the Description page of Project Settings.


#include "Buff.h"

// Sets default values
ABuff::ABuff()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	elapsedTime = 0;

}

// Called when the game starts or when spawned
void ABuff::BeginPlay()
{
	Super::BeginPlay();

}

void ABuff::ExecuteBehavior(float DeltaTime)
{

}

void ABuff::StartBuff()
{
	StartVisualEffects();
}

void ABuff::EndBuff()
{
	EndBuffVisualEffect();
}

// Called every frame
void ABuff::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
	 
	elapsedTime += DeltaTime;
	if (elapsedTime >= buffDuration)
	{
		EndBuff(); // end effects

		// destroy the buff 
		Destroy();
	}
	
	ExecuteBehavior(DeltaTime); // actually might not need this anymore since decided to use a timed function instead to repeat stuff 

}

void ABuff::RefreshBuffDuration()
{
	elapsedTime = 0;
}

void ABuff::DestroyBuff()
{
	EndBuff();
	OnBuffCalledToDestroy.Broadcast(this);
	Destroy();
}

