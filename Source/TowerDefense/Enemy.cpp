// Fill out your copyright notice in the Description page of Project Settings.


#include "Enemy.h"
#include "Components/StaticMeshComponent.h"
#include "Components/BoxComponent.h"
#include "EnemyAiController.h"
#include "HealthComponent.h"
#include "BuffManagerComponent.h"
#include "Runtime/Engine/Classes/Engine/TargetPoint.h"
#include "GameFramework/CharacterMovementComponent.h"

// Sets default values
AEnemy::AEnemy()
{
 	// Set this character to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	CollisionBoxComponent = CreateDefaultSubobject<UBoxComponent>(TEXT("Collision Box Component"));
	CollisionBoxComponent->SetupAttachment(RootComponent);

	StaticMesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("Static Mesh Component"));
	StaticMesh->SetupAttachment(RootComponent);

	EnemyHealthComponent = CreateDefaultSubobject<UHealthComponent>(TEXT("Health Component"));
	BuffManagerComponent = CreateDefaultSubobject<UBuffManagerComponent>(TEXT("Buff Manager Component"));

	waypointIndex = 0;
}

// Called when the game starts or when spawned
void AEnemy::BeginPlay()
{
	Super::BeginPlay();

	SpawnDefaultController();

	// Listen to it's health component
	EnemyHealthComponent->OnOwnerNoHealth.AddDynamic(this, &AEnemy::OnHealthComponentDeath);

	// MoveToNextWaypoint();

	// SPEED SETUP
	ModifySpeed(baseSpeed);
		//speed
}

void AEnemy::MoveToNextWaypoint()
{
	//UE_LOG(LogTemp, Log, TEXT("In move to waypoint function enemy"));

	if (Waypoints.Num() == NULL)
	{
		UE_LOG(LogTemp, Warning, TEXT("waypoitns are null!"));
	}

	if (Waypoints.Num() != NULL)
	{
		if (Waypoints.Num() > 0 && waypointIndex < Waypoints.Num())
		{
			 Cast<AEnemyAiController>(GetController())->MoveToActor(Cast<ATargetPoint>(Waypoints[waypointIndex]));
			waypointIndex++;
		}
		else if (waypointIndex >= Waypoints.Num())
		{
			UE_LOG(LogTemp, Warning, TEXT("Last waypoint reached!"));
		}
	}
}

void AEnemy::OnHealthComponentDeath()
{
	Die();
}

// Called every frame
void AEnemy::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

void AEnemy::Die()
{
	UE_LOG(LogTemp, Warning, TEXT("An enemy has been destroyed, REASON: Out of health"));
	OnEnemyDeath.Broadcast(killReward);
	DestroyEnemy();
}

void AEnemy::DestroyEnemy()
{
	BuffManagerComponent->DestroyAllBuffs();
	Destroy();
}

int32 AEnemy::GetDamage()
{
	return damage;
}

void AEnemy::ModifySpeed(float newSpeed)
{
	speed = newSpeed; // for the sake of getting a reference that is equal to thato f the values of the char move component for the buffsto handle 
	this->GetCharacterMovement()->MaxWalkSpeed = newSpeed;
	this->GetCharacterMovement()->MaxFlySpeed = newSpeed;
}

TEnumAsByte<EType> AEnemy::GetEnemyType()
{
	return EnemyType;
}

// Called to bind functionality to input
void AEnemy::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);

}

UHealthComponent* AEnemy::GetHealthComponent()
{
	return EnemyHealthComponent;
}

void AEnemy::ModifyHealth(float modValue)
{
	// avoid an input of 0
	EnemyHealthComponent->ModifyHealth(modValue);
}

void AEnemy::ModifyKillReward(float modValue)
{
	killReward *= modValue;
}

