// Fill out your copyright notice in the Description page of Project Settings.


#include "EnemyAiController.h"
#include "Enemy.h"

AEnemyAiController::AEnemyAiController()
{

}

void AEnemyAiController::BeginPlay()
{
	Super::BeginPlay();

}

void AEnemyAiController::OnMoveCompleted(FAIRequestID RequestID, const FPathFollowingResult& Result)
{
	Super::OnMoveCompleted(RequestID, Result);

	if (Result.Code == EPathFollowingResult::Success)
	{
		Cast<AEnemy>(GetPawn())->MoveToNextWaypoint();
	}
}


