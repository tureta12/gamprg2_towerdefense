// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Pawn.h"
#include "Player_Object.generated.h"

DECLARE_DYNAMIC_MULTICAST_DELEGATE(FPlayerDeathSignature);

UCLASS()
class TOWERDEFENSE_API APlayer_Object : public APawn
{
	GENERATED_BODY()

public:
	// Sets default values for this pawn's properties
	APlayer_Object();

	UPROPERTY(BlueprintAssignable)
		FPlayerDeathSignature OnPlayerDeath;

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Player Stats")
		int32 health;


	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Player Stats")
		int32 noCoins;

	//// We will not need the Cores and Spawners if the GameMode can facilitate the biding of the player to the events 
	//UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = "Cores")
	//	TArray<AActor*> Cores;

	//UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = "Spawners")
	//	TArray<AActor*> Spawners;

private:
	//UFUNCTION()
	//	void OnEnemySpawned(AEnemy* enemy);

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	// Called to bind functionality to input
	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Player Stats")
		bool isAlive;

	UFUNCTION(BlueprintCallable)
		int32 GetHealth();

	UFUNCTION(BlueprintCallable)
		int32 GetCoins();

	UFUNCTION()
		void AddCoins(int32 newCoins);

	UFUNCTION()
		void SubtractCoins(int32 value);

	UFUNCTION()
		void OnEnemyReachedCore(int32 damage);

	UFUNCTION()
		void OnEnemyDeath(int32 killReward);

};
