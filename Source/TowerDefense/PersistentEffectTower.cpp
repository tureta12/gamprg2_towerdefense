// Fill out your copyright notice in the Description page of Project Settings.


#include "PersistentEffectTower.h"
#include "BuffManagerComponent.h"
#include "Buff.h"

void APersistentEffectTower::ApplyBuff(AActor* target)
{

}

void APersistentEffectTower::RemoveBuff(AActor* target)
{

}

void APersistentEffectTower::StuffToDoBeforeDestroy()
{
	// remove all buffs from enemies in range. to be overriden 
}

void APersistentEffectTower::ExecuteBehavior(float DeltaTime)
{
	Super::ExecuteBehavior(DeltaTime);
}

void APersistentEffectTower::OnActorEnterOverlap(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult)
{

}

void APersistentEffectTower::OnActorExitOverlap(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex)
{

}

void APersistentEffectTower::RefreshBuffsOfTargetsInRange()
{

}

void APersistentEffectTower::ChangePersistentTowerStats()
{

}

void APersistentEffectTower::OnUpgrade()
{
	Super::OnUpgrade();
	ChangePersistentTowerStats();
	RefreshBuffsOfTargetsInRange();
}

void APersistentEffectTower::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
}
