// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "SingleTargetTowers.h"
#include "ProjectileTowers.generated.h"

/**
 * 
 */
UCLASS()
class TOWERDEFENSE_API AProjectileTowers : public ASingleTargetTowers
{
	GENERATED_BODY()
	
public:
	AProjectileTowers();

protected:
	virtual void FindTarget() override;

	virtual void Fire() override;

	virtual void OnUpgrade() override;

	UPROPERTY(EditAnywhere, Category = "Projetile Stats")
		TSubclassOf<class AProjectile> Projectile;

	UFUNCTION()
		virtual void InitSpawnedProjectile(class AProjectile* spawnedProjectile);

private:
	UFUNCTION()
		void SpawnProjectile();

	UPROPERTY(VisibleAnywhere = "Projectile Stats")
		class AProjectile* latestSpawnedProjectile;
};
