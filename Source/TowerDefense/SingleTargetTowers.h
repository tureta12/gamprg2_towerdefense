// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Tower.h"
#include "MyEnums.h"
#include "SingleTargetTowers.generated.h"

/**
 * 
 */
UCLASS()
class TOWERDEFENSE_API ASingleTargetTowers : public ATower
{
	GENERATED_BODY()

public:
	ASingleTargetTowers();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	virtual void ExecuteBehavior(float DeltaTime) override;

	virtual void OnActorEnterOverlap(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp,
			int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult) override;

	virtual void OnActorExitOverlap(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp,
			int32 OtherBodyIndex) override;

	UPROPERTY(VisibleAnywhere, Category = "Target")
		class AEnemy* currentTarget;

	UPROPERTY(EditAnywhere, Category = "Stats")
		TArray<float> damage;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Components")
		class UArrowComponent* ArrowComponent;

	UPROPERTY(EditAnywhere, Category = "Setup")
		bool isMeshDefaultOrientationAbnormal;

	UFUNCTION()
		virtual void FindTarget(); // to be overriden by child classes . Projecetile Towers and then Laser Tower

	UFUNCTION()
		void ResetTarget();

	virtual void Fire() override; // to be overriden by child classes

	virtual void OnUpgrade() override;

	virtual void BehaviorAfterUpgrading() override;

private: 
	UPROPERTY(EditAnywhere, Category = "Target") // enum type
		TArray<TEnumAsByte<EType>> TowerTargetType;



	UFUNCTION()
		void RotateTowardsTarget(float DeltaTime);

public:

};
