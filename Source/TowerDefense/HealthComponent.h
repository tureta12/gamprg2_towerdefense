// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "HealthComponent.generated.h"

DECLARE_DYNAMIC_MULTICAST_DELEGATE(FOnOwnerNoHealth);

UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class TOWERDEFENSE_API UHealthComponent : public UActorComponent
{
	GENERATED_BODY()

public:	
	// Sets default values for this component's properties
	UHealthComponent();

	UPROPERTY(BlueprintAssignable)
		FOnOwnerNoHealth OnOwnerNoHealth;

protected:
	// Called when the game starts
	virtual void BeginPlay() override;

private:
	UPROPERTY(EditAnywhere, Category = "Health")
		float maxHealth;

	UPROPERTY(EditAnywhere, Category = "Health")
		float currentHealth;

	UPROPERTY(VisibleAnywhere, Category = "Owner")
		AActor* owner;

public:	
	UFUNCTION(BlueprintCallable)
		AActor* GetComponentOwner();

	UFUNCTION(BlueprintCallable)
		void TakeDamage(float damage);

	UFUNCTION(BlueprintCallable)
		void ModifyHealth(float value);
};
