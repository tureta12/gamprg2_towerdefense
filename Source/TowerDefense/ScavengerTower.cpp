// Fill out your copyright notice in the Description page of Project Settings.


#include "ScavengerTower.h"
#include "Player_Object.h"
#include "Enemy.h"

void AScavengerTower::BeginPlay()
{
	Super::BeginPlay();

	// player obj ref 

	StartFiring();
}

void AScavengerTower::Fire()
{
	// gold stuff
	playerReference->AddCoins(passiveGold[level]);
}

void AScavengerTower::OnUpgrade()
{
	Super::OnUpgrade();

	playerReference->AddCoins(passiveGold[level]);
}

void AScavengerTower::OnEnemyDeath(int32 enemyKillReward)
{
	playerReference->AddCoins(enemyKillReward * extraGoldRate[level]);
}

void AScavengerTower::OnActorEnterOverlap(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult)
{
	if (AEnemy* target = Cast<AEnemy>(OtherActor))
	{
		if (!ActorsInRange.Contains(target))
		{
			ActorsInRange.Add(target);
			target->OnEnemyDeath.AddDynamic(this, &AScavengerTower::OnEnemyDeath);
		}
	}
}

void AScavengerTower::OnActorExitOverlap(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex)
{
	if (AEnemy* target = Cast<AEnemy>(OtherActor))
	{
		if (ActorsInRange.Contains(target))
		{
			ActorsInRange.Remove(target);
			target->OnEnemyDeath.RemoveDynamic(this, &AScavengerTower::OnEnemyDeath);
		}
	}
}
