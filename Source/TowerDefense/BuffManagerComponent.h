// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "BuffManagerComponent.generated.h"


UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class TOWERDEFENSE_API UBuffManagerComponent : public UActorComponent
{
	GENERATED_BODY()

public:	
	// Sets default values for this component's properties
	UBuffManagerComponent();

protected:
	// Called when the game starts
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;

	UPROPERTY(VisibleAnywhere)
		TArray<class ABuff*> listOfBuffs;

	// only putting these 2 in cause i think it will be expensive to cast every time 
	UPROPERTY(VisibleAnywhere)
		TArray<class ASlowBuff*> slowBuffs;

	UPROPERTY(VisibleAnywhere)
		TArray<class AOverloadBuff*> overloadBuffs;

	UFUNCTION()
		void DestroyAllBuffs(); // to be called before destroying an enemy or tower. although bufs will destory themselves after some duration. this will be more memory friendly 

	//UFUNCTION()
	//	void DestroyBuffsOfName(FString nameOfBuff); // called by emp tower before it's destroyed to remove the slow buff from enemies in it's range. Also power generator 
	
	UFUNCTION()
		bool IsBuffInManager(FString nameOfBuff); // search through list of buffs and return whether or not name of buff is present 

	UFUNCTION()
		ABuff* GetBuffReference(FString nameOfBuff); // used by laser tower
	
	UFUNCTION()
		void FindBuffAndDestroy(TArray<class ABuff*> towerBuffList);

	UFUNCTION()
		ABuff* SpawnBuff(TSubclassOf<class ABuff>  buff, float nBuffStrength);

	UFUNCTION()
		void SpawnTickBuff(TSubclassOf<class ABuff>  buff, float nBuffStrength, float buffDurationMulti);

	UFUNCTION()
		void EndBuffs();

	UFUNCTION()
		void StartBuffs();

	UFUNCTION()
		void OnBuffDestroyed(class ABuff* buff);

};
