// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "BuildManager.generated.h"

DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FTowerSpawnSignature, class ATower*, tower);

UCLASS()
class TOWERDEFENSE_API ABuildManager : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	ABuildManager();

	UPROPERTY(BlueprintAssignable)
		FTowerSpawnSignature OnTowerSpawn;

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

private:
	UPROPERTY()
		FTimerHandle PlayerFindTimerHandle;

	UPROPERTY(VisibleAnywhere)
		TArray<class AActor*> PlayerObjectHolders;

	UPROPERTY(VisibleAnywhere)
		class APlayer_Object* PlayerObject;

	UPROPERTY(EditAnywhere)
		class UTowerData* CurrentTowerData; // Make this VisibleAnywhere after testing if can build 

	UPROPERTY()
		bool IsGoldEnough;

	UFUNCTION()
		void FindPlayerObject(); // called after some delay to ensure that the player object is indeed spawned

	UFUNCTION(BlueprintCallable)
		void ExitBuildMode();

	UFUNCTION(BlueprintCallable)
		void BuildTower();


public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	UPROPERTY(EditAnywhere)
		TArray<class UTowerData*> TowerDatas;

	UPROPERTY(VisibleAnywhere, BlueprintReadWrite)
		bool BuildModeActive;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		class ATowerGhost* TowerGhost; // direct reference to an actor in the scene 

	UFUNCTION(BlueprintCallable)
		void ActivateGhostTower(class UTowerData* towerData);

	UFUNCTION(BlueprintImplementableEvent)
		void MoveGhostTowerToMouse();


};
