// Fill out your copyright notice in the Description page of Project Settings.


#include "Player_Core.h"
#include "Enemy.h"
#include "Components/StaticMeshComponent.h"
#include "Components/BoxComponent.h"

// Sets default values
APlayer_Core::APlayer_Core()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	StaticMesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("Static Mesh Component"));
	StaticMesh->SetupAttachment(RootComponent);

	CollisionBoxComponent = CreateDefaultSubobject<UBoxComponent>(TEXT("Collision Box Component"));
	CollisionBoxComponent->SetupAttachment(StaticMesh);
}

// Called when the game starts or when spawned
void APlayer_Core::BeginPlay()
{
	Super::BeginPlay();

	StaticMesh->OnComponentBeginOverlap.AddDynamic(this, &APlayer_Core::OnMeshBeginOverlap);
	
}

void APlayer_Core::OnMeshBeginOverlap(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult)
{
	UE_LOG(LogTemp, Warning, TEXT("Something collided with the CORE"));
	if (AEnemy* target = Cast<AEnemy>(OtherActor))
	{
		UE_LOG(LogTemp, Warning, TEXT("Enemy collided with CORE"));

		EnemyReachedCore.Broadcast(target->GetDamage());
		
		UE_LOG(LogTemp, Warning, TEXT("Enemy Destroyed, REASON: Reached Core"));
		target->DestroyEnemy();
	}
}


// Called every frame
void APlayer_Core::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

