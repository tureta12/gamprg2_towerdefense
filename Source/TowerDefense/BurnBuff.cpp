// Fill out your copyright notice in the Description page of Project Settings.


#include "BurnBuff.h"
#include "Enemy.h"
#include "HealthComponent.h"

void ABurnBuff::DoTickEffect()
{
	Super::DoTickEffect();

	// take target's health component and deal damage of the buff 
	Cast<AEnemy>(target)->GetHealthComponent()->TakeDamage(buffStrength);
}
