// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "PersistentEffectTower.h"
#include "PowerGeneratorTower.generated.h"

/**
 * 
 */
UCLASS()
class TOWERDEFENSE_API APowerGeneratorTower : public APersistentEffectTower
{
	GENERATED_BODY()
	
private:
	virtual void ApplyBuff(AActor* target) override;

	virtual void RemoveBuff(AActor* target) override;

	virtual void StuffToDoBeforeDestroy() override;

	virtual void ExecuteBehavior(float DeltaTime) override; // maybe to be overriden by power generator for on demand collision checking 

	virtual void OnActorEnterOverlap(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp,
		int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult) override;

	virtual void OnActorExitOverlap(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp,
		int32 OtherBodyIndex) override;

	virtual void Init(class ABuildManager* buildManager, int32 goldCost, FString name) override;

	void RefreshBuffsOfTargetsInRange() override;

	//void ChangePersistentTowerStats() override;

	UFUNCTION()
		void CheckForNewTower(class ATower* tower);

public:
	// Called every frame
	virtual void Tick(float DeltaTime) override; // potential use could be Power Generator for on demand collision checking 

};
