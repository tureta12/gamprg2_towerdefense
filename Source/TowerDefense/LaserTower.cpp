// Fill out your copyright notice in the Description page of Project Settings.


#include "LaserTower.h"
#include "Enemy.h"
#include "Buff.h"
#include "BurnBuff.h"
#include "BuffManagerComponent.h"

void ALaserTower::FindTarget()
{
	// Neaerest enemy that ISn't BURNING. If everyone' is burnign then just go to nearest enemy to tower or just stay at the last target
	// code is similar to that of projectile towers FindTarget() but now we consider the presence of the buff 
	AEnemy* NoBuffYetTarget = NULL;
	AEnemy* closestTarget = NULL; // this the target the tower will shoot at if all enemies have buffs already 

	if (ActorsInRange.Num() != NULL && ActorsInRange.Num() > 0)
	{
		if (ActorsInRange.Num() == 1)
		{
			closestTarget = Cast<AEnemy>(ActorsInRange[0]);

			if (Cast<AEnemy>(ActorsInRange[0])->BuffManagerComponent->IsBuffInManager(BurnBuff->GetDefaultObject<ABuff>()->buffName))
			{
				return;
			}
			// we will only reach this point if the enemy doesn't have a burn buff already 
			NoBuffYetTarget = Cast<AEnemy>(ActorsInRange[0]);
		}
		else
		{
			// find the nearest enemy in the list of actors in range and make that the target if no burn buff
			for (int i = 0; i < ActorsInRange.Num() - 1; i++)
			{
				float actor_1_Distance = GetDistanceTo(ActorsInRange[i]);
				float actor_2_Distance = GetDistanceTo(ActorsInRange[i + 1]);

				if (actor_1_Distance < actor_2_Distance)
				{
					closestTarget = Cast<AEnemy>(ActorsInRange[i]);

					if (!Cast<AEnemy>(ActorsInRange[i])->BuffManagerComponent->IsBuffInManager(BurnBuff->GetDefaultObject<ABuff>()->buffName))
					{
						NoBuffYetTarget = Cast<AEnemy>(ActorsInRange[i]);
					}

				}
				else
				{
					closestTarget = Cast<AEnemy>(ActorsInRange[i + 1]);

					if (!Cast<AEnemy>(ActorsInRange[i + 1])->BuffManagerComponent->IsBuffInManager(BurnBuff->GetDefaultObject<ABuff>()->buffName))
					{
						NoBuffYetTarget = Cast<AEnemy>(ActorsInRange[i + 1]);
					}
				}
			}
		}
		if (NoBuffYetTarget != NULL) // If all enemies don't have buffs, then just make the target the closestTarget to the tower 
		{
			currentTarget = NoBuffYetTarget;
		}
		else if (closestTarget != NULL)
		{
			currentTarget = closestTarget;
		}
	}
}

void ALaserTower::Fire()
{
	Super::Fire();

	// If the buff is present in the target just refresh
	if (ABuff* theBuff = currentTarget->BuffManagerComponent->GetBuffReference(BurnBuff->GetDefaultObject<ABuff>()->buffName))
	{
		if (theBuff != NULL)
		{
			theBuff->RefreshBuffDuration();
		}
	}
	else // if not, then spawn a new buff. Buff manager handles the init and adding to itself
	{
		currentTarget->BuffManagerComponent->SpawnTickBuff(BurnBuff, damage[level], BuffDurationMulti[level]);
	}

	// After firing, always reset target since we want to find another enemy, preferably one who doesn't have the burn buff yet 
	ResetTarget();
}

void ALaserTower::OnUpgrade()
{
	Super::OnUpgrade();

}
