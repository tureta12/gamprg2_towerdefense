// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Buff.h"
#include "TickBuff.generated.h"

/**
 * 
 */
UCLASS()
class TOWERDEFENSE_API ATickBuff : public ABuff
{
	GENERATED_BODY()
	
protected:
	virtual void BeginPlay() override;

	UPROPERTY(VisibleAnywhere)
		FTimerHandle DelayBetweenTicks;

	UPROPERTY(EditAnywhere)
		float tickRate;

	UFUNCTION()
		virtual void DoTickEffect();

	virtual void ExecuteBehavior(float DeltaTime) override;
};
