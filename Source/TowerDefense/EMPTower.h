// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "PersistentEffectTower.h"
#include "EMPTower.generated.h"

/**
 * 
 */
UCLASS()
class TOWERDEFENSE_API AEMPTower : public APersistentEffectTower
{
	GENERATED_BODY()

private:
	UPROPERTY(EditAnywhere, Category = "Stats")
		TArray<float> TowerRangeMulti;

		virtual void ApplyBuff(AActor* target) override;

		virtual void RemoveBuff(AActor* target) override;

		virtual void StuffToDoBeforeDestroy() override;

	virtual void ExecuteBehavior(float DeltaTime) override; // maybe to be overriden by power generator for on demand collision checking 

	virtual void OnActorEnterOverlap(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp,
		int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult) override;

	virtual void OnActorExitOverlap(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp,
		int32 OtherBodyIndex) override;

	void RefreshBuffsOfTargetsInRange() override;

	void ChangePersistentTowerStats() override;

public:
	// Called every frame
	virtual void Tick(float DeltaTime) override; // potential use could be Power Generator for on demand collision checking 
};
