// Fill out your copyright notice in the Description page of Project Settings.


#include "TurretProjectile.h"
#include "HealthComponent.h"
#include "Enemy.h"

void ATurretProjectile::OnProjectileOverlapEffect(AActor* otherActor)
{
	// Turret "hit" behavior 
	if (AEnemy* hitActor = Cast<AEnemy>(otherActor))
	{
		hitActor->GetHealthComponent()->TakeDamage(damage);
	}
}