// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "StaticBuff.h"
#include "OverloadBuff.generated.h"

/**
 * 
 */
UCLASS()
class TOWERDEFENSE_API AOverloadBuff : public AStaticBuff
{
	GENERATED_BODY()
	
public:
		virtual void StartBuff() override;

		virtual void EndBuff() override;
};
