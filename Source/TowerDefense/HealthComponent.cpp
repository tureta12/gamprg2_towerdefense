// Fill out your copyright notice in the Description page of Project Settings.


#include "HealthComponent.h"

// Sets default values for this component's properties
UHealthComponent::UHealthComponent()
{
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
	PrimaryComponentTick.bCanEverTick = false;

	// ...
}


// Called when the game starts
void UHealthComponent::BeginPlay()
{
	Super::BeginPlay();

	currentHealth = maxHealth;

	owner = GetOwner();

}

AActor* UHealthComponent::GetComponentOwner()
{
	return owner;
}

void UHealthComponent::TakeDamage(float damage)
{
	if (damage <= 0)
	{
		// do nothing
		UE_LOG(LogTemp, Warning, TEXT("Health component. received no damage!"));
		return;
	}

	currentHealth = FMath::Clamp(currentHealth - damage, 0.0f, maxHealth);
	UE_LOG(LogTemp, Warning, TEXT("Helath comp. received damage!"));

	if (currentHealth <= 0)
	{
		// dead 
		OnOwnerNoHealth.Broadcast();
	}
}

void UHealthComponent::ModifyHealth(float value)
{
	currentHealth *= value;
}
