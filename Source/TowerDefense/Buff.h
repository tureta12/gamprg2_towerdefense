// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Buff.generated.h"

DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FBuffToDestroySignature, ABuff*, buff);

UCLASS()
class TOWERDEFENSE_API ABuff : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	ABuff();

	UPROPERTY(BlueprintAssignable)
		FBuffToDestroySignature OnBuffCalledToDestroy;

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	UFUNCTION()
		virtual void ExecuteBehavior(float DeltaTime);

private:
	UPROPERTY(VisibleAnywhere)
	float elapsedTime;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	UPROPERTY(EditAnywhere)
		FString buffName;

	UPROPERTY(EditAnywhere)
		float buffStrength; // a variable for damage, slow, etc. 

	UPROPERTY(VisibleAnywhere, BlueprintReadWrite)
		class AActor* target;

	UPROPERTY(EditAnywhere)
		float buffDuration;

	UFUNCTION()
		void RefreshBuffDuration();

	UFUNCTION()
		void DestroyBuff();

	UFUNCTION()
		virtual void StartBuff();

	UFUNCTION()
		virtual void EndBuff();

	UFUNCTION(BlueprintImplementableEvent)
		void StartVisualEffects(); // put like a repeating emitter in BP maybe. Or just idicate buffs via a widget above enemies instead?

	UFUNCTION(BlueprintImplementableEvent)
		void EndBuffVisualEffect();

};
