// Fill out your copyright notice in the Description page of Project Settings.


#include "Player_Object.h"
#include "Player_Core.h"
#include "SpawnPoint.h"
#include "Enemy.h"
#include "Runtime/Engine/Classes/Kismet/GameplayStatics.h"

// Sets default values
APlayer_Object::APlayer_Object()
{
 	// Set this pawn to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	//health = 20;
	isAlive = true;
	//noCoins = 50;
}

// Called when the game starts or when spawned
void APlayer_Object::BeginPlay()
{
	Super::BeginPlay();
	
/*  // not needed since we do the binding of the player in the game mode already
	// WE won't need to do any of the thigns below if game mode can facilitate giving the event

	//// Find all the cores 
	//UGameplayStatics::GetAllActorsOfClass(GetWorld(), APlayer_Core::StaticClass(), Cores);

	//// Listen
	//if (Cores.Num() != NULL && Cores.Num() > 0)
	//{
	//	for (int i = 0; i < Cores.Num(); i++)
	//	{
	//		Cast<APlayer_Core>(Cores[i])->EnemyReachedCore.AddDynamic(this, &APlayer_Object::OnEnemyReachedCore);
	//	}
	//}

	//// Find all spawners then listen
	//UGameplayStatics::GetAllActorsOfClass(GetWorld(), ASpawnPoint::StaticClass(), Cores);
	//if (Spawners.Num() != NULL && Spawners.Num() > 0)
	//{
	//	for (int i = 0; i < Spawners.Num(); i++)
	//	{
	//		Cast<ASpawnPoint>(Spawners[i])->OnEnemySpawn.AddDynamic(this, &APlayer_Object::OnEnemySpawned);
	//	}
	//}
	*/ 
}

// Called every frame
void APlayer_Object::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

// Called to bind functionality to input
void APlayer_Object::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);

}

void APlayer_Object::OnEnemyReachedCore(int32 damage)
{
	health -= damage;
	if (health <= 0)
	{
		health = 0; // have a min cap for UI purposes
		// out of health stuff
		UE_LOG(LogTemp, Warning, TEXT("Player is out of health!"));
		isAlive = false;

		// GAME OVER STUFF
		OnPlayerDeath.Broadcast();
	}
}

//void APlayer_Object::OnEnemySpawned(AEnemy* enemy)
//{
//	enemy->OnEnemyDeath.AddDynamic(this, &APlayer_Object::OnEnemyDeath);
//}

void APlayer_Object::OnEnemyDeath(int32 killReward)
{
	AddCoins(killReward);
}

int32 APlayer_Object::GetHealth()
{
	return health;
}

int32 APlayer_Object::GetCoins()
{
	return noCoins;
}

void APlayer_Object::AddCoins(int32 newCoins)
{
	noCoins += newCoins;
}

void APlayer_Object::SubtractCoins(int32 value)
{
	noCoins -= value;

	if (noCoins < 0)
	{
		noCoins = 0;
	}
}



