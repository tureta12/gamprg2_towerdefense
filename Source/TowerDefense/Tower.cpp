// Fill out your copyright notice in the Description page of Project Settings.


#include "Tower.h"
#include "Enemy.h"
#include "Player_Object.h"
#include "BuffManagerComponent.h"
#include "Components/StaticMeshComponent.h"
#include "Components/BoxComponent.h"
#include "Components/SphereComponent.h"
#include "Components/SceneComponent.h"
#include "Engine/StaticMesh.h"

// Sets default values
ATower::ATower()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	SceneComponent = CreateDefaultSubobject<USceneComponent>(TEXT("Root Scene Component"));
	RootComponent = SceneComponent;

	BaseStaticMesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("Base Mesh Component"));
	BaseStaticMesh->SetupAttachment(RootComponent);

	HeadStaticMesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("Head Mesh Component"));
	HeadStaticMesh->SetupAttachment(BaseStaticMesh);

	SphereComponent = CreateDefaultSubobject<USphereComponent>(TEXT("Sphere Component"));
	SphereComponent->SetupAttachment(RootComponent);

	BoxComponent = CreateDefaultSubobject<UBoxComponent>(TEXT("Collision Box Component"));
	BoxComponent->SetupAttachment(RootComponent);

	BuffManagerComponent = CreateDefaultSubobject<UBuffManagerComponent>(TEXT("Buff Manager Component"));

	maxLevelIndex = 2; // I ONLY ADDED THIS AND IT ERRORED LIKE krezy
	isFiring = false;
	level = 0;
}

int32 ATower::GetGoldNeededForUpgrade()
{
	return TowerBaseStats.GoldCostToUpgrade[level];
}

// Called when the game starts or when spawned
void ATower::BeginPlay()
{
	Super::BeginPlay();
	
	// Overlaps
	SphereComponent->OnComponentBeginOverlap.AddDynamic(this, &ATower::OnActorEnterOverlap);
	SphereComponent->OnComponentEndOverlap.AddDynamic(this, &ATower::OnActorExitOverlap);

	BoxComponent->OnClicked.AddDynamic(this, &ATower::OnTowerClickedFunction);

	// get player ref cuz used a lot
	playerReference = Cast<APlayer_Object>(GetWorld()->GetFirstPlayerController()->GetPawn());

	if (TowerBaseStats.FireRate.Num() != NULL)
	{
		fireRate = TowerBaseStats.FireRate[level];
	}
}

void ATower::ExecuteBehavior(float DeltaTime)
{

}

void ATower::StartFiring()
{
	if (isFiring != true)
	{
		GetWorldTimerManager().SetTimer(FiringTimerHandle, this, &ATower::Fire, fireRate, true);
		isFiring = true;
	}
}

void ATower::StuffToDoBeforeDestroy()
{

}

void ATower::OnActorEnterOverlap(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult)
{

}

void ATower::OnActorExitOverlap(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex)
{

}

bool ATower::DoesPlayerHaveEnoughGoldToUpgrade()
{
	// to be called by UI
	if (playerReference->GetCoins() >= TowerBaseStats.GoldCostToUpgrade[level])
	{
		return true;
	}
	else
	{
		return false;
	}

	//return true; // remove this after test
}

void ATower::Upgrade()
{
	if ((level + 1) > maxLevelIndex)
	{
		UE_LOG(LogTemp, Warning, TEXT("TOWER at max level oolredy!"));
		return;
	}

	UE_LOG(LogTemp, Warning, TEXT("TOWER UPGRADEUUH!"));

	// reduce player's gold
	playerReference->SubtractCoins(TowerBaseStats.GoldCostToUpgrade[level]);

	// add to tower's total gold cost 
	totalGoldWorth += TowerBaseStats.GoldCostToUpgrade[level];

	// increment level 
	level++;

	// base behavior is to just modify the base mesh and static mesh 
	HeadStaticMesh->SetStaticMesh(TowerBaseStats.HeadStaticMeshes[level]);
	BaseStaticMesh->SetStaticMesh(TowerBaseStats.BaseStaticMeshes[level]);

	OnUpgrade();

	BehaviorAfterUpgrading();

	BuffManagerComponent->StartBuffs();
}

void ATower::OnUpgrade()
{
	// to be overriden 
}

void ATower::BehaviorAfterUpgrading()
{
	// to beo verriden by singletarget towers
}

void ATower::SellTower()
{
	UE_LOG(LogTemp, Warning, TEXT("TOWER SEULLUH!"));

	playerReference->AddCoins(totalGoldWorth / 2.0f);
	DestroyTower();
}

void ATower::OnTowerClickedFunction(UPrimitiveComponent* pComponent, FKey inKey)
{
	UE_LOG(LogTemp, Warning, TEXT("TOWER WAS CLICKEUDUFDUFSUFDUF"));

	OnTowerClicked.Broadcast(this);
}

void ATower::Fire()
{
	// UE_LOG(LogTemp, Warning, TEXT("Firing!"));
	Effects();
}

// Called every frame
void ATower::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	ExecuteBehavior(DeltaTime);

}

//void ATower::ModifyFireRate(float value)
//{
//	fireRate *= value;
//}

void ATower::Init(ABuildManager* buildManager, int32 goldCost, FString name)
{
	totalGoldWorth = goldCost;
	TowerName = name;
}

void ATower::DestroyTower()
{
	BuffManagerComponent->DestroyAllBuffs();
	StuffToDoBeforeDestroy();
	OnTowerDestroyed.Broadcast();
	Destroy();
}

void ATower::ModifyFireRate(float newFireRate)
{
	fireRate = newFireRate;
}
