// Copyright Epic Games, Inc. All Rights Reserved.


#include "TowerDefenseGameModeBase.h"
#include "WaveData.h"
#include "SpawnPoint.h"
#include "Enemy.h"
#include "BuildManager.h"
#include "Tower.h"
#include "Player_Object.h"
#include "Player_Core.h"
#include "Runtime/Engine/Classes/Kismet/GameplayStatics.h"


ATowerDefenseGameModeBase::ATowerDefenseGameModeBase()
{
	waveNumber = 0;
}

void ATowerDefenseGameModeBase::BeginPlay()
{
	Super::BeginPlay();

	// The player_object then listen to on death event
	thePlayer = Cast<APlayer_Object>(GetWorld()->GetFirstPlayerController()->GetPawn());
	thePlayer->OnPlayerDeath.AddDynamic(this, &ATowerDefenseGameModeBase::OnPlayerDeath);

	// Find Cores
	UGameplayStatics::GetAllActorsOfClass(GetWorld(), APlayer_Core::StaticClass(), Cores);

	// loop through the cores and listen to the event enemy reached core 
	if (Cores.Num() != NULL && Cores.Num() > 0)
	{
		UE_LOG(LogTemp, Warning, TEXT("There are cores found!"));
		for (int i = 0; i < Cores.Num(); i++)
		{
			Cast<APlayer_Core>(Cores[i])->EnemyReachedCore.AddDynamic(this, &ATowerDefenseGameModeBase::OnEnemyReachedCore);

			// Also bind the player to the event
			Cast<APlayer_Core>(Cores[i])->EnemyReachedCore.AddDynamic(thePlayer, &APlayer_Object::OnEnemyReachedCore);

			UE_LOG(LogTemp, Warning, TEXT("Listening to core!"));
		}
	}

	// Find Spawners
	UGameplayStatics::GetAllActorsOfClass(GetWorld(), ASpawnPoint::StaticClass(), Spawners);

	// loop through the spawners and listen to the enemy spawn event
	if (Spawners.Num() != NULL && Spawners.Num() > 0)
	{
		UE_LOG(LogTemp, Warning, TEXT("There are spawners found!"));
		for (int i = 0; i < Spawners.Num(); i++)
		{
			Cast<ASpawnPoint>(Spawners[i])->OnEnemySpawn.AddDynamic(this, &ATowerDefenseGameModeBase::OnEnemySpawned);
			Cast<ASpawnPoint>(Spawners[i])->OnReadyForNextSubwave.AddDynamic(this, &ATowerDefenseGameModeBase::OnReadyForNextSubwaveSpawnerUpdate);
			UE_LOG(LogTemp, Warning, TEXT("Listening to spawner!"));
		}
	}

	if (WaveDatas.Num() != NULL && WaveDatas.Num() > 0)
	{
		GiveSpawnersWaveData(WaveDatas[waveNumber]); // Wave 1
	}

	// Build Manager Ref
	UGameplayStatics::GetAllActorsOfClass(GetWorld(), ABuildManager::StaticClass(), BuildManagerRefs);
	Cast<ABuildManager>(BuildManagerRefs[0])->OnTowerSpawn.AddDynamic(this, &ATowerDefenseGameModeBase::OnBuildManagerSpawnedTower);

	// Once Found, we call the BlueprintImplementablEvent to give the UI a reference to the BuildManager. zzzz so complicated of a work around
	GiveBuildManagerRefToUI();
}

int32 ATowerDefenseGameModeBase::GetMaxWaves()
{
	return WaveDatas.Num();
}

int32 ATowerDefenseGameModeBase::GetCurrentWaveNumber()
{
	return waveNumber;
}

int32 ATowerDefenseGameModeBase::GetTotalNumberEnemiesInWave()
{
	return totalNumberEnemiesInWave;
}

int32 ATowerDefenseGameModeBase::GetTotalDestroyedEnemiesInWave()
{
	return totalDestroyedEnemiesInWave;
}

void ATowerDefenseGameModeBase::GiveSpawnersWaveData(UWaveData *waveData)
{
	if (WaveDatas.Num() != NULL && WaveDatas.Num() > 0)
	{
		for (int i = 0; i < Spawners.Num(); i++)
		{
			Cast<ASpawnPoint>(Spawners[i])->currentWaveData = waveData;
			Cast<ASpawnPoint>(Spawners[i])->StartWaveAfterDelay();
		}

		// figure out how many enemies are in da wave 
		for (int i = 0; i < waveData->EnemySpawnData.Num(); i++)
		{
			totalNumberEnemiesInWave += (waveData->EnemySpawnData[i].Spawners.Num() * waveData->EnemySpawnData[i].numberOfEnemies);
		}

		UE_LOG(LogTemp, Warning, TEXT("Finished GiveSpawnersWaveData!"));
		// UE_LOG(LogTemp, Log, TEXT("Number of enemies in next wave: %s"), totalNumberEnemiesInWave); ENABLING THIS CAUSES A NULL ERROR -__-
	}
}

void ATowerDefenseGameModeBase::OnWaveEnd()
{
	// Clear Reward
	thePlayer->AddCoins(WaveDatas[waveNumber]->clearReward);

	//Cast<APlayer_Object>(GetWorld()->GetFirstPlayerController()->GetPawn()).AddCoins(WaveDatas[waveNumber]->clearReward);

	waveNumber++; // Increment wave number
	totalDestroyedEnemiesInWave = 0; // reset the count
	totalNumberEnemiesInWave = 0; // also reset

	if (waveNumber < WaveDatas.Num() && thePlayer->isAlive) // only continue with next wave if player is still alive 
	{
		GiveSpawnersWaveData(WaveDatas[waveNumber]);
	}
	else
	{
		UE_LOG(LogTemp, Log, TEXT("No more waves!"));

		// game finished stuff. DO NOT EXECUTE THEM IF THE PLAYER HAS <= 0 HEALTH. 
		if (thePlayer->isAlive)
		{
			// GAME WON!
			OnGameWin(); // blueprint implementable 
		}
		else if (!thePlayer->isAlive)
		{
			// GAME OVER. leave this blank. game mode already listens to the player's on death event (blueprint implementable) and will automatically exeute 
		}
	}
}

void ATowerDefenseGameModeBase::OnReadyForNextSubwaveSpawnerUpdate()
{
	bool ready = false;

	for (int i = 0; i < Spawners.Num(); i++)
	{
		ready = Cast<ASpawnPoint>(Spawners[i])->IsReadyForNextSubwave();
		if (ready == false)
		{
			UE_LOG(LogTemp, Warning, TEXT("One of the spawners isn't ready for the next subwave!"));
			return;
		}
	}

	UE_LOG(LogTemp, Warning, TEXT("Spawners are ready for the next subwave"));

	// if it gets to this point, then it assumes that all spawners are ready for the next subwave
	for (int i = 0; i < Spawners.Num(); i++)
	{
		Cast<ASpawnPoint>(Spawners[i])->StartNextSubwave();
	}
}

void ATowerDefenseGameModeBase::OnEnemySpawned(AEnemy* enemy)
{
	enemy->OnEnemyDeath.AddDynamic(this, &ATowerDefenseGameModeBase::OnEnemyDeath);

	// Also do the same for player
	enemy->OnEnemyDeath.AddDynamic(Cast<APlayer_Object>(thePlayer), &APlayer_Object::OnEnemyDeath);

	// UE_LOG(LogTemp, Warning, TEXT("game mode and player now listening to just spawned enemy!"));
}

void ATowerDefenseGameModeBase::OnEnemyReachedCore(int32 damage)
{
	totalDestroyedEnemiesInWave++;

	if (totalDestroyedEnemiesInWave >= totalNumberEnemiesInWave)
	{
		OnWaveEnd();
	}
}

void ATowerDefenseGameModeBase::OnEnemyDeath(int32 reward)
{
	totalDestroyedEnemiesInWave++;

	if (totalDestroyedEnemiesInWave >= totalNumberEnemiesInWave)
	{
		OnWaveEnd();
	}
}

ABuildManager* ATowerDefenseGameModeBase::ReturnBuildManagerRef()
{
	if (BuildManagerRefs.Num() != NULL && BuildManagerRefs.Num() > 0)
	{
		return Cast<ABuildManager>(BuildManagerRefs[0]);
	}
	return nullptr;
}

void ATowerDefenseGameModeBase::OnBuildManagerSpawnedTower(ATower* tower)
{
	// LISTEN TO SPAWNED TOWER
	tower->OnTowerClicked.AddDynamic(this, &ATowerDefenseGameModeBase::OnTowerClicked);
}

void ATowerDefenseGameModeBase::OnTowerClicked(ATower* tower)
{
	// blueprint implementable where we take game mode's latest clicked tower variable and pass that over to the TOWER UI before adding it to the viewport 
	latestClickedTower = tower;
	GiveTowerReferenceToTowerMenu();
}
