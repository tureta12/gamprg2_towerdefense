// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "BuildableNode.generated.h"

UCLASS()
class TOWERDEFENSE_API ABuildableNode : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	ABuildableNode();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

private:
	UPROPERTY(VisibleAnywhere)
		class UBoxComponent* TowerBuildZone;

		UPROPERTY(VisibleAnywhere)
		class UStaticMeshComponent* StaticMesh; 

		UFUNCTION()
			void OnTowerDestroyed();
public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	UPROPERTY(VisibleAnywhere)
		class ATower* currentTower;

	UFUNCTION()
		void Init(class ATower* newTower);

};
