// Fill out your copyright notice in the Description page of Project Settings.


#include "EMPTower.h"
#include "BuffManagerComponent.h"
#include "Enemy.h"
#include "Buff.h"
#include "Components/SphereComponent.h"

void AEMPTower::ApplyBuff(AActor* target)
{
	if (!(target == NULL))
	{
		if (ABuff* spawnedBuff = Cast<AEnemy>(target)->BuffManagerComponent->SpawnBuff(theBuff, nBuffStrength[level]))
		{
			if (spawnedBuff != NULL)
			{
				listOfSpawnedBuffs.Add(spawnedBuff);
			}
			else
			{
				UE_LOG(LogTemp, Warning, TEXT("SPAWNED BUFF WAS NULL?!"));
			}
		}
	}
	else
	{
		UE_LOG(LogTemp, Warning, TEXT("HOW THE TARGET BECOME NULL?!"));
	}
}

void AEMPTower::RemoveBuff(AActor* target)
{
	//Cast<AEnemy>(target)->BuffManagerComponent->DestroyBuffsOfName(theBuff->GetDefaultObject<ABuff>()->buffName);
	Cast<AEnemy>(target)->BuffManagerComponent->FindBuffAndDestroy(listOfSpawnedBuffs);
}

void AEMPTower::StuffToDoBeforeDestroy()
{
	if (ActorsInRange.Num() != NULL)
	{
		for (int i = 0; i < ActorsInRange.Num(); i++)
		{
			//Cast<AEnemy>(ActorsInRange[i])->BuffManagerComponent->DestroyBuffsOfName(theBuff->GetDefaultObject<ABuff>()->buffName);
			Cast<AEnemy>(ActorsInRange[i])->BuffManagerComponent->FindBuffAndDestroy(listOfSpawnedBuffs);
		}
	}
}

void AEMPTower::ExecuteBehavior(float DeltaTime)
{
	Super::ExecuteBehavior(DeltaTime);
}

void AEMPTower::OnActorEnterOverlap(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult)
{
	if (AEnemy* target = Cast<AEnemy>(OtherActor))
	{
		if (!ActorsInRange.Contains(target))
		{
			ActorsInRange.Add(target);
			ApplyBuff(target);
		}
	}
}

void AEMPTower::OnActorExitOverlap(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex)
{
	if (AEnemy* target = Cast<AEnemy>(OtherActor))
	{
		if (ActorsInRange.Contains(target))
		{
			ActorsInRange.Remove(target);
			RemoveBuff(target);
		}
	}
}

void AEMPTower::RefreshBuffsOfTargetsInRange()
{
	if (ActorsInRange.Num() != NULL)
	{
		for (int i = 0; i < ActorsInRange.Num(); i++)
		{
			Cast<AEnemy>(ActorsInRange[i])->BuffManagerComponent->FindBuffAndDestroy(listOfSpawnedBuffs);
			ApplyBuff(ActorsInRange[i]);
		}
	}
}

void AEMPTower::ChangePersistentTowerStats()
{
	Super::ChangePersistentTowerStats();

	// increasde da range of the sphere collider or AOE
	SphereComponent->SetSphereRadius(SphereComponent->GetUnscaledSphereRadius() * TowerRangeMulti[level]);

// On demand collision check here since the range of the sphere increased 
	TArray<AActor*> Result;
	GetOverlappingActors(Result, AEnemy::StaticClass());

	// compare it to the current list  of targets
	for (int i = 0; i < Result.Num(); i++)
	{
		if (!ActorsInRange.Contains(Result[i]))
		{
			AActor* newTarget = Result[i];
			ApplyBuff(newTarget);
			ActorsInRange.Add(newTarget);
		}
	}
}

void AEMPTower::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}