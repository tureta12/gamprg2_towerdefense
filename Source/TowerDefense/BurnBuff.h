// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "TickBuff.h"
#include "BurnBuff.generated.h"

/**
 * 
 */
UCLASS()
class TOWERDEFENSE_API ABurnBuff : public ATickBuff
{
	GENERATED_BODY()
	
private:
	void DoTickEffect() override;

public:
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		UMaterial* burningMaterial;
};
