////// Fill out your copyright notice in the Description page of Project Settings.


#include "PowerGeneratorTower.h"
#include "BuffManagerComponent.h"
#include "BuildManager.h"
#include "SingleTargetTowers.h"
#include "Buff.h"

void APowerGeneratorTower::ApplyBuff(AActor* target)
{
	listOfSpawnedBuffs.Add(Cast<ASingleTargetTowers>(target)->BuffManagerComponent->SpawnBuff(theBuff, nBuffStrength[level]));
}

void APowerGeneratorTower::RemoveBuff(AActor* target)
{
	Cast<ASingleTargetTowers>(target)->BuffManagerComponent->FindBuffAndDestroy(listOfSpawnedBuffs);
}

void APowerGeneratorTower::StuffToDoBeforeDestroy()
{
	if (ActorsInRange.Num() != NULL)
	{
		for (int i = 0; i < ActorsInRange.Num(); i++)
		{
			Cast<ASingleTargetTowers>(ActorsInRange[i])->BuffManagerComponent->FindBuffAndDestroy(listOfSpawnedBuffs);
		}
	}
}

void APowerGeneratorTower::ExecuteBehavior(float DeltaTime)
{
	Super::ExecuteBehavior(DeltaTime);
}

void APowerGeneratorTower::OnActorEnterOverlap(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult)
{
	if (ASingleTargetTowers* target = Cast<ASingleTargetTowers>(OtherActor))
	{
		if (!ActorsInRange.Contains(target))
		{
			ActorsInRange.Add(target);
			ApplyBuff(target);
		}
	}
}

void APowerGeneratorTower::OnActorExitOverlap(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex)
{
	if (ATower* target = Cast<ASingleTargetTowers>(OtherActor))
	{
		if (ActorsInRange.Contains(target))
		{
			ActorsInRange.Remove(target);
			RemoveBuff(target);
		}
	}
}

void APowerGeneratorTower::Init(class ABuildManager* buildManager, int32 goldCost, FString name)
{
	Super::Init(buildManager, goldCost, name);

	// listen to the buildManager
	buildManager->OnTowerSpawn.AddDynamic(this, &APowerGeneratorTower::CheckForNewTower);
}

void APowerGeneratorTower::CheckForNewTower(ATower* tower)
{
	// On demand collision check here
	TArray<AActor*> Result;
	GetOverlappingActors(Result, ASingleTargetTowers::StaticClass());

	// compare it to the current list  of targets
	for (int i = 0; i < Result.Num(); i++)
	{
		if (!ActorsInRange.Contains(Result[i]))
		{
			ActorsInRange.Add(Result[i]);
			ApplyBuff(Result[i]);
		}
	}
}

void APowerGeneratorTower::RefreshBuffsOfTargetsInRange()
{
	if (ActorsInRange.Num() != NULL)
	{
		for (int i = 0; i < ActorsInRange.Num(); i++)
		{
			Cast<ASingleTargetTowers>(ActorsInRange[i])->BuffManagerComponent->FindBuffAndDestroy(listOfSpawnedBuffs);
			ApplyBuff(ActorsInRange[i]);
		}
	}
}

//void APowerGeneratorTower::ChangePersistentTowerStats()
//{
//	Super::ChangePersistentTowerStats();
//
//	// nothing 
//
//}

void APowerGeneratorTower::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	// On demand collision check? nvm check CheckForNewTower() instead

}