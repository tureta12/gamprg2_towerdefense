// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "WaveData.h"
#include "SpawnPoint.generated.h"

DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FEnemySpawnSignature, AEnemy*, spawnedEnemy);
DECLARE_DYNAMIC_MULTICAST_DELEGATE(FSpawnPointReadyForNextSubwaveSignature);

UCLASS()
class TOWERDEFENSE_API ASpawnPoint : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	ASpawnPoint();

	UPROPERTY(BlueprintAssignable)
		FEnemySpawnSignature OnEnemySpawn;

	UPROPERTY(BlueprintAssignable)
		FSpawnPointReadyForNextSubwaveSignature OnReadyForNextSubwave;

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

private:
	UPROPERTY()
		FTimerHandle DelayBetweenSpawnsTimerHandle;

	UPROPERTY()
		FTimerHandle DelayBetweenWavesTimerHandle;

	UPROPERTY(VisibleAnywhere, Category = "Live Stats")
	int32 enemySpawnDataIndex;

	UPROPERTY(VisibleAnywhere, Category = "Live Stats")
		int32 indexOfSimilarSpawnerName;

	UPROPERTY(VisibleAnywhere, Category = "Live Stats")
	int32 noEnemiesInSpawnData;

	UPROPERTY(VisibleAnywhere, Category = "Live Stats")
	int32 spawnedEnemiesInSpawnData;

	UPROPERTY(VisibleAnywhere, Category = "Setup")
		bool readyForNextSubwave;

	UPROPERTY(EditAnywhere, Category = "Setup")
		float delayBetweenSpawns;

	UPROPERTY(EditAnywhere, Category = "Setup")
		float delayBetweenSubwaves;

	UPROPERTY(EditAnywhere, Category = "Setup")
		float delayBetweenWaves;

	UPROPERTY(VisibleAnywhere)
		ACharacter* latestSpawnedEnemy;

	UFUNCTION()
		void StartWave();

	UFUNCTION()
		void ExecuteSubwave();

	UFUNCTION()
		void HandleSpawning();

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	UPROPERTY(EditAnywhere)
		FString spawnPointName;

	UPROPERTY(EditAnywhere, Category = "Movement Targets")
		TArray<AActor*>Waypoints;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Components")
		class UArrowComponent* ArrowComponent;

	UPROPERTY(VisibleAnywhere)
		UWaveData* currentWaveData;

	UFUNCTION()
		void StartWaveAfterDelay();

	UFUNCTION()
		FString GetName();

	UFUNCTION()
		bool IsReadyForNextSubwave();

	UFUNCTION()
		void StartNextSubwave();
};
