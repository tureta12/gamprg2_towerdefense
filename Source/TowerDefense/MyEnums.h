// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "MyEnums.generated.h"

/**
 * 
 */
UENUM(BlueprintType)
enum EType
{
	TYPE_Ground	UMETA(DisplayName = "Ground"),
	TYPE_Flying UMETA(DisplayName = "Flying"),
	TYPE_Boss   UMETA(DisplayName = "Boss")
};

class TOWERDEFENSE_API MyEnums
{
public:
	MyEnums();
	~MyEnums();
};
