// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Engine/DataAsset.h"
#include "WaveData.generated.h"

/**
 * 
 */

USTRUCT(BlueprintType)
struct FEnemySpawnData
{
	GENERATED_BODY()

	UPROPERTY(EditAnywhere, BlueprintReadOnly)
		TSubclassOf<class AEnemy> Enemy;

	UPROPERTY(EditAnywhere, BlueprintReadOnly)
		int32 numberOfEnemies;

	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	TArray<TSubclassOf<class ASpawnPoint>> Spawners;
	//TArray<TSubclassOf<class AActor>> Spawners;


};

UCLASS(BlueprintType)
class TOWERDEFENSE_API UWaveData : public UDataAsset
{
	GENERATED_BODY()
	
public:
	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	float duration; // of the wave?

	//UPROPERTY(EditAnywhere, BlueprintReadOnly) // TESTING PURPOSES
 //   TSubclassOf<class ACharacter> Enemy;

	UPROPERTY(EditAnywhere, BlueprintReadOnly)
    TArray<FEnemySpawnData> EnemySpawnData;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Reward")
	float killRewardMultiplier;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Additional Stats")
	float enemyHealthMultiplier;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Reward")
	int32 clearReward;

	UFUNCTION()
    AEnemy* GetEnemyClass(int32 indexOfSubwave);

	//UFUNCTION(BlueprintPure, BlueprintCallable)
	//int32 TotalKillReward(); // not to be used, just a template in case i want to put a pure function here 
};
