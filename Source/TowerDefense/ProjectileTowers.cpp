// Fill out your copyright notice in the Description page of Project Settings.


#include "ProjectileTowers.h"
#include "Projectile.h"
#include "Components/ArrowComponent.h"
#include "BuffManagerComponent.h"
#include "Enemy.h"

AProjectileTowers::AProjectileTowers()
{
	// damage = 0;
}

void AProjectileTowers::FindTarget()
{
	// targeting behavior of Turrent and Missle Tower 

	AEnemy* tempTarget = NULL;

	if (ActorsInRange.Num() != NULL && ActorsInRange.Num() > 0)
	{
		if (ActorsInRange.Num() == 1)
		{
			tempTarget = Cast<AEnemy>(ActorsInRange[0]);
		}
		else
		{
			// find the nearest enemy in the list of actors in range and make that the target
			for (int i = 0; i < ActorsInRange.Num() - 1; i++)
			{
				float actor_1_Distance = GetDistanceTo(ActorsInRange[i]);
				float actor_2_Distance = GetDistanceTo(ActorsInRange[i + 1]);

				if (actor_1_Distance < actor_2_Distance)
				{
					tempTarget = Cast<AEnemy>(ActorsInRange[i]);
				}
				else
				{
					tempTarget = Cast<AEnemy>(ActorsInRange[i + 1]);
				}
			}
		}
		if (tempTarget != NULL)
		{
			currentTarget = tempTarget;
		}
	}
}

void AProjectileTowers::Fire()
{
	Super::Fire();

	SpawnProjectile();
}

void AProjectileTowers::OnUpgrade()
{
	Super::OnUpgrade();

	// Reapply the buffs that were on the tower to account for any new data (eg fire rate of turret) 
	// We need to end them first. 
	BuffManagerComponent->EndBuffs();

	// update fire rate
	fireRate = TowerBaseStats.FireRate[level];

	// then reapply the buffss 
	BuffManagerComponent->StartBuffs();

}

void AProjectileTowers::InitSpawnedProjectile(AProjectile* spawnedProjectile)
{
	latestSpawnedProjectile->InitProjectile(damage[level], currentTarget, ArrowComponent->GetForwardVector());
}

void AProjectileTowers::SpawnProjectile()
{
	// Spawn stuff
	FActorSpawnParameters spawnParams;
	spawnParams.SpawnCollisionHandlingOverride = ESpawnActorCollisionHandlingMethod::AlwaysSpawn;
	spawnParams.bNoFail = true;
	spawnParams.Owner = this;

	if (Projectile != NULL)
	{
		latestSpawnedProjectile = GetWorld()->SpawnActor<AProjectile>(Projectile, ArrowComponent->GetComponentTransform(), spawnParams);

		InitSpawnedProjectile(latestSpawnedProjectile);
		// Inits
		// latestSpawnedProjectile->InitProjectile(damage, currentTarget, ArrowComponent->GetComponentTransform().Rotator().Vector());

		UE_LOG(LogTemp, Warning, TEXT("A projectile was just spawned!"));

	}
}