// Fill out your copyright notice in the Description page of Project Settings.


#include "OverloadBuff.h"
#include "Tower.h"

void AOverloadBuff::StartBuff()
{
	Super::StartBuff();
	float towerFireRate = Cast<ATower>(target)->fireRate;
	float modifiedFireRate = towerFireRate * buffStrength;

	Cast<ATower>(target)->ModifyFireRate(modifiedFireRate);
}

void AOverloadBuff::EndBuff()
{
	if (isActivated == true)
	{
		Super::EndBuff();
		float towerFireRate = Cast<ATower>(target)->fireRate;
		float modifiedFireRate = towerFireRate / buffStrength;

		Cast<ATower>(target)->ModifyFireRate(modifiedFireRate);
	}
}