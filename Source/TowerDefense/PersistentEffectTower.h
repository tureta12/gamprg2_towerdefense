// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Tower.h"
#include "PersistentEffectTower.generated.h"

/**
 * 
 */
UCLASS()
class TOWERDEFENSE_API APersistentEffectTower : public ATower
{
	GENERATED_BODY()
	
protected:
	UPROPERTY(EditAnywhere)
		TSubclassOf<class ABuff>  theBuff;

	UPROPERTY(EditAnywhere, Category = "Stats")
		TArray<float> nBuffStrength;
	
	UPROPERTY(VisibleAnywhere, Category = "Stats")
		TArray<class ABuff*> listOfSpawnedBuffs;

	UFUNCTION()
		virtual void RefreshBuffsOfTargetsInRange();

	UFUNCTION()
		virtual void ChangePersistentTowerStats();

	UFUNCTION()
		virtual void ApplyBuff(AActor* target);

	UFUNCTION()
		virtual void RemoveBuff(AActor* target);

		virtual void StuffToDoBeforeDestroy() override;

	virtual void ExecuteBehavior(float DeltaTime) override; // maybe to be overriden by power generator for on demand collision checking 

		virtual void OnActorEnterOverlap(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp,
			int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult) override;

		virtual void OnActorExitOverlap(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp,
			int32 OtherBodyIndex) override;

		void OnUpgrade() override;
public:
	// Called every frame
	virtual void Tick(float DeltaTime) override; // potential use could be Power Generator for on demand collision checking 

};
