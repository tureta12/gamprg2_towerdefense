// Fill out your copyright notice in the Description page of Project Settings.


#include "BuildableNode.h"
#include "TowerGhost.h"
#include "Tower.h"
#include "Components/StaticMeshComponent.h"
#include "Components/BoxComponent.h"

// Sets default values
ABuildableNode::ABuildableNode()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	StaticMesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("Static Mesh Component"));
	RootComponent = StaticMesh;

	TowerBuildZone = CreateDefaultSubobject<UBoxComponent>(TEXT("TowerBuildZone Box Component"));
	TowerBuildZone->SetupAttachment(RootComponent);
}

// Called when the game starts or when spawned
void ABuildableNode::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void ABuildableNode::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

void ABuildableNode::Init(ATower* newTower)
{
	currentTower = newTower;
	currentTower->OnTowerDestroyed.AddDynamic(this, &ABuildableNode::OnTowerDestroyed);
}

void ABuildableNode::OnTowerDestroyed()
{
	currentTower = NULL;
}

