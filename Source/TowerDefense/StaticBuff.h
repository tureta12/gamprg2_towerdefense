// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Buff.h"
#include "StaticBuff.generated.h"

/**
 * 
 */
UCLASS()
class TOWERDEFENSE_API AStaticBuff : public ABuff
{
	GENERATED_BODY()
	
public:
	AStaticBuff();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

		virtual void StartBuff() override;

		virtual void EndBuff() override;

public:
	UPROPERTY(VisibleAnywhere, Category = "Stats")
		bool isActivated;
};
