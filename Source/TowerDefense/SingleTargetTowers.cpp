// Fill out your copyright notice in the Description page of Project Settings.


#include "SingleTargetTowers.h"
#include "Components/ArrowComponent.h"
#include "Enemy.h"
#include "Kismet/KismetMathLibrary.h"

ASingleTargetTowers::ASingleTargetTowers()
{
	ArrowComponent = CreateDefaultSubobject<UArrowComponent>("Arrow Component");
	ArrowComponent->SetupAttachment(HeadStaticMesh);

	isMeshDefaultOrientationAbnormal = false;
}

void ASingleTargetTowers::BeginPlay()
{
	Super::BeginPlay();
}

void ASingleTargetTowers::ExecuteBehavior(float DeltaTime)
{
	Super::ExecuteBehavior(DeltaTime);

	// if no current target
	if (currentTarget == NULL)
	{
		ResetTarget(); // security
		FindTarget();
	}
	else if (currentTarget)
	{
		RotateTowardsTarget(DeltaTime);

		if (isFiring == false)
		{
			// once rotated, fire at it !
			StartFiring();
		}
	}
}

void ASingleTargetTowers::OnActorEnterOverlap(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult)
{
	if (AActor* target = Cast<AEnemy>(OtherActor))
	{
		// add em to the list of enemies in range
		AEnemy* tempEnemy = Cast<AEnemy>(target);

		// check if compatible with target type first. only add if not already in the array
		if (TowerTargetType.Contains(tempEnemy->GetEnemyType()) && !ActorsInRange.Contains(target))
		{
			ActorsInRange.Add(target);
		}
	}
}

void ASingleTargetTowers::OnActorExitOverlap(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex)
{
	if (AActor* target = Cast<AEnemy>(OtherActor))
	{
		if (target == currentTarget)
		{
			// reset target
			ResetTarget();
		}

		// remove from list of actors in range
		if (ActorsInRange.Contains(target))
		{
			ActorsInRange.Remove(target);
		}
	}
}

void ASingleTargetTowers::Fire()
{
	Super::Fire();
	// Fire stuff. blank. to be overriden 

}

void ASingleTargetTowers::OnUpgrade()
{
	Super::OnUpgrade();

}


void ASingleTargetTowers::BehaviorAfterUpgrading()
{
	Super::BehaviorAfterUpgrading();

	// stop the firing handle 
	GetWorldTimerManager().ClearTimer(FiringTimerHandle);
	isFiring = false;

	// then shoot again with the new fire rate. This is after the buff from the power generator (if present) is reapplied.  
	StartFiring();
}

void ASingleTargetTowers::FindTarget()
{
	// to be overriden by child classes. 
}

void ASingleTargetTowers::RotateTowardsTarget(float DeltaTime)
{
	// rotate the turret mesh towards the target
	if (currentTarget != NULL)
	{
		FVector currentTargetLoc = currentTarget->StaticMesh->GetComponentLocation();

		FRotator rotator = UKismetMathLibrary::FindLookAtRotation(HeadStaticMesh->GetComponentLocation(), currentTargetLoc);

		if (isMeshDefaultOrientationAbnormal == true) // cuz turrent's default orientations does not align with UE4s 
		{
			FRotator tempRotator = rotator;

			// Default forward of the turrent is UE4's Y axis. Need to adjust kek. 
			rotator.Pitch = tempRotator.Roll;
			rotator.Roll = -tempRotator.Pitch;

			rotator.Roll += 5; // for some reason, this needs some offset as the turrent always seems to aim a bit above the enemy's location...
			rotator.Yaw -= 90; // to account for the offset of the HeadStaticMesh. I didn't bother changing it's  default Z axis value or Yaw. 

			//HeadStaticMesh->SetRelativeRotation(rotator); other method
			//FVector forward = currentTarget->ActorToWorld().GetLocation() - HeadStaticMesh->GetComponentTransform().GetLocation(); /*works but no Y axis rota or it's not enough*/
			//FRotator rot = UKismetMathLibrary::MakeRotFromXZ(forward, FVector::ForwardVector);
			////rot.Yaw -= 90;
			//HeadStaticMesh->SetWorldRotation(rot);
		}

		HeadStaticMesh->SetWorldRotation(rotator);
	}
}

void ASingleTargetTowers::ResetTarget()
{
	GetWorldTimerManager().ClearTimer(FiringTimerHandle);
	currentTarget = NULL;
	isFiring = false;
}
