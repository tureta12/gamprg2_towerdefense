// Fill out your copyright notice in the Description page of Project Settings.


#include "BuildManager.h"
#include "TowerGhost.h"
#include "TowerData.h"
#include "Tower.h"
#include "BuildableNode.h"
#include "Runtime/Engine/Classes/Kismet/GameplayStatics.h"
#include "Player_Object.h"

// Sets default values
ABuildManager::ABuildManager()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	BuildModeActive = false;
}

// Called when the game starts or when spawned
void ABuildManager::BeginPlay()
{
	Super::BeginPlay();
	
	GetWorldTimerManager().SetTimer(PlayerFindTimerHandle, this, &ABuildManager::FindPlayerObject, 1, false); // not looping
}

void ABuildManager::FindPlayerObject()
{
	UGameplayStatics::GetAllActorsOfClass(GetWorld(), APlayer_Object::StaticClass(), PlayerObjectHolders);

	PlayerObject = Cast<APlayer_Object>(PlayerObjectHolders[0]);
}

void ABuildManager::ExitBuildMode()
{
	// called by RMB via Input. Make sure to only call this when BuildMode == true in the BP 
	TowerGhost->DisableGhostTower();
	BuildModeActive = false;
}

void ABuildManager::BuildTower()
{
	// called by LMB via Input. Make sure to only call this function in the BP if BuildMode == true

	if (TowerGhost->canBuildInLocation)
	{
		// Get player's current gold and compare to gold cost
		if (PlayerObject->GetCoins() >= CurrentTowerData->goldCost)
		{

			// If enough, then SPAWN a TOWER and do whatever Init if necessary 
			FActorSpawnParameters spawnParams;
			spawnParams.SpawnCollisionHandlingOverride = ESpawnActorCollisionHandlingMethod::AlwaysSpawn;
			spawnParams.bNoFail = true;
			spawnParams.Owner = this;

			if (CurrentTowerData != NULL)
			{
				FTransform spawnTransform = TowerGhost->GetActorTransform();
				FVector vector;

				// reset scale to prevent scale multiplication
				vector.X = 1;
				vector.Y = 1;
				vector.Z = 1;
				spawnTransform.SetScale3D(vector);

				ATower* spawnedTower = GetWorld()->SpawnActor<ATower>(CurrentTowerData->Tower, spawnTransform, spawnParams);
				// Init
				spawnedTower->Init(this, CurrentTowerData->goldCost, CurrentTowerData->towerName);
				OnTowerSpawn.Broadcast(spawnedTower);

				// Also give the BUILDABLE NODE a reference to the NEWLY SPAWNED TOWER
				TowerGhost->currentNodeHovered->Init(spawnedTower);

				// Subtract from the player's current gold 
				PlayerObject->SubtractCoins(CurrentTowerData->goldCost);
			}

			ExitBuildMode();
		}
		else
		{
			// need some UI indicator saying NOT ENOUGH GOLD or someting 
		}
	}
}

// Called every frame
void ABuildManager::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
	
	if (BuildModeActive)
	{
		// then always move the tower ghost reference to the position dictated by the mouse cursor
		// movement chu chu here 
		MoveGhostTowerToMouse();
	}
}

void ABuildManager::ActivateGhostTower(class UTowerData* towerData)
{
	// UI button click must CALL THIS FUNCTION 

	BuildModeActive = true;
	CurrentTowerData = towerData;

	// Init 
	TowerGhost->SetTowerGhostData(towerData);
}



