// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "ProjectileTowers.h"
#include "MissleTower.generated.h"

/**
 * 
 */
UCLASS()
class TOWERDEFENSE_API AMissleTower : public AProjectileTowers
{
	GENERATED_BODY()
	
protected:
	UPROPERTY(EditAnywhere, Category = "Stats")
		TArray<float> MissleSplashRadiusMulti;

	virtual void OnUpgrade() override;

	void InitSpawnedProjectile(class AProjectile* spawnedProjectile) override;

};
