// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Tower.generated.h"

USTRUCT(BlueprintType)
struct FTowerBaseStatsData
{
	GENERATED_BODY()

	UPROPERTY(EditAnywhere, BlueprintReadOnly)
		TArray<float> FireRate;

	UPROPERTY(EditAnywhere, BlueprintReadOnly)
		TArray<int32> GoldCostToUpgrade; // meaning the last index of this won't be used 

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Stats")
		TArray<class UStaticMesh*> HeadStaticMeshes;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Stats")
		TArray<class UStaticMesh*> BaseStaticMeshes;

	//UPROPERTY(EditAnywhere, BlueprintReadOnly)
	//	TArray<FString> testingStringIfYouShowUPTHenWFTF;
};

DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FTowerClickSignature, ATower*, tower);
DECLARE_DYNAMIC_MULTICAST_DELEGATE(FTowerDeathSignature);

UCLASS()
class TOWERDEFENSE_API ATower : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	ATower();

	UPROPERTY(BlueprintAssignable)
		FTowerClickSignature OnTowerClicked;

	UPROPERTY(BlueprintAssignable)
		FTowerDeathSignature OnTowerDestroyed;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Stats")
		FTowerBaseStatsData TowerBaseStats;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Stats")
		int32 totalGoldWorth;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Stats")
		int32 level;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Stats")
		int32 maxLevelIndex;

	UFUNCTION(BlueprintCallable)
		int32 GetGoldNeededForUpgrade();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	UPROPERTY(VisibleAnywhere, Category = "Stats")
		class APlayer_Object* playerReference;

	UPROPERTY()
		FTimerHandle FiringTimerHandle;

	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = "Stats")
		TArray<AActor*> ActorsInRange;

	UPROPERTY(VisibleAnywhere, Category = "Stats")
		bool isFiring;



	UFUNCTION()
	virtual void ExecuteBehavior(float DeltaTime);

	UFUNCTION()
		void StartFiring(); // turrret, missle tower, laser tower, and scavenger(?)

	UFUNCTION()
		virtual void StuffToDoBeforeDestroy();

	UFUNCTION()
		virtual void Fire();

	UFUNCTION() 
		virtual void OnActorEnterOverlap(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, 
			int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult);

	UFUNCTION()
		virtual void OnActorExitOverlap(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp,
			int32 OtherBodyIndex);

	UFUNCTION(BlueprintCallable)
		bool DoesPlayerHaveEnoughGoldToUpgrade();

	UFUNCTION(BlueprintCallable)
		virtual void Upgrade();

	UFUNCTION()
		virtual void OnUpgrade();

	UFUNCTION()
		virtual void BehaviorAfterUpgrading();

	UFUNCTION(BlueprintCallable)
		void SellTower();

private:
	UFUNCTION()
		void OnTowerClickedFunction(UPrimitiveComponent* pComponent, FKey inKey);

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	//UFUNCTION()
	//	void ModifyFireRate(float value);

	UPROPERTY(VisibleAnywhere, Category = "Components")
		class UStaticMeshComponent* HeadStaticMesh;

	UPROPERTY(VisibleAnywhere, Category = "Components")
		class UStaticMeshComponent* BaseStaticMesh;

	UPROPERTY(VisibleAnywhere, Category = "Components")
		class USphereComponent* SphereComponent;

	UPROPERTY(VisibleAnywhere, Category = "Components")
		class UBoxComponent* BoxComponent;

	UPROPERTY(VisibleAnywhere, Category = "Components")
		class USceneComponent* SceneComponent;

	UPROPERTY(VisibleAnywhere, Category = "Components")
		class UBuffManagerComponent* BuffManagerComponent;

	UPROPERTY(VisibleAnywhere, Category = "Stats")
		float fireRate;

	UPROPERTY(VisibleAnywhere, Category = "Stats")
		float baseFireRate;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Stats")
		FString TowerName;

	UFUNCTION()
		virtual void DestroyTower();

	UFUNCTION(BlueprintImplementableEvent)
		void Effects();

	UFUNCTION()
		void ModifyFireRate(float newFireRate);

	UFUNCTION()
		virtual void Init(class ABuildManager* buildManager, int32 goldCost, FString name);

};
