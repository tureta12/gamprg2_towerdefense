// Fill out your copyright notice in the Description page of Project Settings.


#include "BuffManagerComponent.h"
#include "Buff.h"
#include "StaticBuff.h"
#include "SlowBuff.h"
#include "OverloadBuff.h"

// Sets default values for this component's properties
UBuffManagerComponent::UBuffManagerComponent()
{
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
	PrimaryComponentTick.bCanEverTick = true;

	// ...
}


// Called when the game starts
void UBuffManagerComponent::BeginPlay()
{
	Super::BeginPlay();

	// ...
	
}


// Called every frame
void UBuffManagerComponent::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);

	// ...
}

void UBuffManagerComponent::DestroyAllBuffs()
{
	TArray<ABuff*> listOfBuffsToDestroy;

	if (listOfBuffs.Num() != NULL)
	{
		for (int i = 0; i < listOfBuffs.Num(); i++)
		{
			listOfBuffsToDestroy.Add(listOfBuffs[i]);
		}
	}

	if (listOfBuffsToDestroy.Num() != NULL)
	{
		if (listOfBuffsToDestroy.Num() > 0)
		{
			for (int i = listOfBuffsToDestroy.Num(); i > 0; i--) // start from the back to avoid null errors caused by resizing 
			{
				// remove it from the listOfBuffs first.OnBuffDestroyed handles this now
				//listOfBuffs.Remove(listOfBuffsToDestroy[i - 1]);
				ABuff* buffToDestroy = listOfBuffsToDestroy[i - 1];
				listOfBuffsToDestroy.Remove(buffToDestroy);
				buffToDestroy->DestroyBuff();
			}
		}
	}
}

//void UBuffManagerComponent::DestroyBuffsOfName(FString nameOfBuff)
//{
//	TArray<ABuff*> listOfBuffsToDestroy;
//
//	if (listOfBuffs.Num() != NULL)
//	{
//		for (int i = 0; i < listOfBuffs.Num(); i++)
//		{
//			if (nameOfBuff == listOfBuffs[i]->buffName)
//			{
//				listOfBuffsToDestroy.Add(listOfBuffs[i]);
//			}
//		}
//	}
//
//	if (listOfBuffsToDestroy.Num() != NULL)
//	{
//		if (listOfBuffsToDestroy.Num() > 0)
//		{
//			for (int i = listOfBuffsToDestroy.Num(); i > 0; i--) // start from the back to avoid null errors caused by resizing 
//			{
//				// remove it from the list of buffs. OnBuffDestroyed handles this now
//				//listOfBuffs.Remove(listOfBuffsToDestroy[i - 1]);
//
//				// then destroy it 
//				ABuff* buffToDestroy = listOfBuffsToDestroy[i - 1];
//				listOfBuffsToDestroy.Remove(buffToDestroy);
//				buffToDestroy->DestroyBuff();
//			}
//		}
//	}
//}

bool UBuffManagerComponent::IsBuffInManager(FString nameOfBuff)
{
	if (listOfBuffs.Num() != NULL)
	{
		for (int i = 0; i < listOfBuffs.Num(); i++)
		{
			if (nameOfBuff == listOfBuffs[i]->buffName)
			{
				return true;
			}
		}
	}

	return false;
}

ABuff* UBuffManagerComponent::GetBuffReference(FString nameOfBuff)
{
	if (listOfBuffs.Num() != NULL)
	{
		for (int i = 0; i < listOfBuffs.Num(); i++)
		{
			if (nameOfBuff == listOfBuffs[i]->buffName)
			{
				return listOfBuffs[i];
			}
		}
	}
	return NULL;
}

void UBuffManagerComponent::FindBuffAndDestroy(TArray<class ABuff*> towerBuffList)
{
	// Used by Persistent Towers (EMP and Power Generator). They are the only ones using this hence i'm casting AStaticBuff below. dirty solution. 
	// This will work under the assumption that each tower only gives exactly ONE buff to each Target.
	// The destroying part also works under the assumption that here can only be ONE buff with it's isActivated set to TRUE at a given time. 
	if (listOfBuffs.Num() != NULL && towerBuffList.Num() != NULL)
	{
		for (int i = 0; i < towerBuffList.Num(); i++)
		{
			if (listOfBuffs.Contains(towerBuffList[i]))
			{
				AStaticBuff* buffToDestroy = Cast<AStaticBuff>(towerBuffList[i]);
				// FIRST IDENTIFY IF IT's SLOW BUFF OR AN OVERLOAD BUFF AND REMOVE SAID BUFF FROM THEIR RESPECTIVE LIST 
				if (ASlowBuff* slow = Cast<ASlowBuff>(buffToDestroy))
				{
					slowBuffs.Remove(slow);
				}
				else if (AOverloadBuff* overload = Cast<AOverloadBuff>(buffToDestroy))
				{
					overloadBuffs.Remove(overload);
				}

				// THEN WE IDENTIFY IF IT'S ACTIVATED OR NOT AND ACT ACCORDINGLY 
				if (buffToDestroy->isActivated == true)
				{
					// IF THE BUFF TO DESTROY IS ACTIVATED THEN THAT MEANS WE HAVE TO ACTIVATE ANOTHER BUFF AFTER DESTROYING IT 
					if (Cast<ASlowBuff>(buffToDestroy))
					{
						// IF THERE AREN't ANY SLOW BUFFS IN THE LIST LEFT (meaning the one that we just removed is the last slow buff)
						// THEN JUST REMOVE THEN DELETE THE BUFF 
						if (slowBuffs.Num() == NULL || slowBuffs.Num() == 0)
						{
							towerBuffList.Remove(buffToDestroy);
							buffToDestroy->DestroyBuff();
						}
						else
						{
							// INIT. IF THE FOR LOOP DOESN'T EXECUTE, THEN IT MEANS THERE IS ONLY (1) BUFF LEFT. 
							ASlowBuff* nextStrongestBuff = slowBuffs[0];
							// OTHERWISE LOOP THROUGH THE RESPECTIVE LIST TO FIND THE NEXT STRONGEST BUFF
							// WE WORK UNDER THE ASSUMPTION THAT EVERYTHING IN THIS LIST isActivated == false
							for (int j = 0; j < slowBuffs.Num() - 1; j++)
							{
								if (slowBuffs[j]->buffStrength > slowBuffs[j + 1]->buffStrength)
								{
									nextStrongestBuff = slowBuffs[j];
								}
								else
								{
									nextStrongestBuff = slowBuffs[j + 1];
								}
							}

							// ONCE A NEW BUFF TO ACTIVATE IS FOUND
							// THEN WE CAN REMOVE THEN DELETE buffToDestroy
							// WE DO THIS FIRST BECAUSE WE WANT THE STAT (SPEED or FIRE RATE TO RESET TO THE ORIGINAL VALUE FIRST)
							// BEFORE ACTIVATING THE NEXT STRONGEST BUFF 
							towerBuffList.Remove(buffToDestroy);
							buffToDestroy->DestroyBuff();

							// THEN WE ACTIVATE THE OTHER BUFF 
							nextStrongestBuff->isActivated = true;
							nextStrongestBuff->StartBuff();
						}
					}
					// OVERLOAD
					// IF THE BUFF TO DESTROY IS ACTIVATED THEN THAT MEANS WE HAVE TO ACTIVATE ANOTHER BUFF AFTER DESTROYING IT 
					else if (Cast<AOverloadBuff>(buffToDestroy))
					{
						// IF THERE AREN't ANY OVERLOAD BUFFS IN THE LIST LEFT (meaning the one that we just removed is the last overload buff)
						// THEN JUST REMOVE THEN DELETE THE BUFF 
						if (overloadBuffs.Num() == NULL || overloadBuffs.Num() == 0)
						{
							towerBuffList.Remove(buffToDestroy);
							buffToDestroy->DestroyBuff();
						}
						else
						{
							// INIT. IF THE FOR LOOP DOESN'T EXECUTE, THEN IT MEANS THERE IS ONLY (1) BUFF LEFT. 
							AOverloadBuff* nextStrongestBuff = overloadBuffs[0];
							// OTHERWISE LOOP THROUGH THE RESPECTIVE LIST TO FIND THE NEXT STRONGEST BUFF
							// WE WORK UNDER THE ASSUMPTION THAT EVERYTHING IN THIS LIST isActivated == false
							for (int j = 0; j < overloadBuffs.Num() - 1; j++)
							{
								if (overloadBuffs[j]->buffStrength > overloadBuffs[j + 1]->buffStrength)
								{
									nextStrongestBuff = overloadBuffs[j];
								}
								else
								{
									nextStrongestBuff = overloadBuffs[j + 1];
								}
							}

							// ONCE A NEW BUFF TO ACTIVATE IS FOUND
							// THEN WE CAN REMOVE THEN DELETE buffToDestroy
							// WE DO THIS FIRST BECAUSE WE WANT THE STAT (SPEED or FIRE RATE TO RESET TO THE ORIGINAL VALUE FIRST)
							// BEFORE ACTIVATING THE NEXT STRONGEST BUFF 
							towerBuffList.Remove(buffToDestroy);
							buffToDestroy->DestroyBuff();

							// THEN WE ACTIVATE THE OTHER BUFF 
							nextStrongestBuff->isActivated = true;
							nextStrongestBuff->StartBuff();
						}
					}
				}
				else
				{
					// ELSE, SIMPLY REMOVE AND DELETE IT 
					towerBuffList.Remove(buffToDestroy);
					buffToDestroy->DestroyBuff();
				}

				// the OnBuffDestroyed function here will automatically remove said buff from this buffManager's list (destroy buff event from Buff.cpp will broadcast even which OnBuffDestroyed listens to)
				return;
			}
		}
	}
}

ABuff* UBuffManagerComponent::SpawnBuff(TSubclassOf<class ABuff> buff, float nBuffStrength)
{
	// Spawn stuff
	FActorSpawnParameters spawnParams;
	spawnParams.SpawnCollisionHandlingOverride = ESpawnActorCollisionHandlingMethod::AlwaysSpawn;
	spawnParams.bNoFail = true;
	// spawnParams.Owner = this;

	FTransform spawnTransform;

	if (buff != NULL)
	{
		AStaticBuff* spawnedBuff = GetWorld()->SpawnActor<AStaticBuff>(buff, spawnTransform, spawnParams);

		// whenever a buff manager component spawns a buff, it will add it to its buff list 
		spawnedBuff->target = this->GetOwner();
		spawnedBuff->buffStrength = nBuffStrength;

		// very dirty code here, but can't think of anyway else to facilitate it so that buffs will not stack
		// we must check if there are any static buffs already activated. if the buffStrenght of the newly spawned one is less than exising ones, then don't activate it. 

		// IF ITS A SLOW BUFF
		if (ASlowBuff* newSlow = Cast<ASlowBuff>(spawnedBuff))
		{
			if (slowBuffs.Num() == NULL || slowBuffs.Num() == 0)
			{
				// IF THERE ARE NO EXISTING BUFFS THEN ACTIVATE 
				newSlow->isActivated = true;
				newSlow->StartBuff();
			}
			else
			{
				ASlowBuff* strongestSlowBuff = slowBuffs[0];
				// IF THE # OF THE LIST ISN'T ZERO. THEN THAT MEANS THERE IS AN ACTIVATAED BUFF OF THIS TYPE. GET THE CURRENT ACTIVATED STRONGEST BUFF
				// WHAT'S HELPING US HERE IS THAT THERE CAN ONLY BE ONE ACTIVE BUFF AT A TIME. 
				for (int i = 0; i < slowBuffs.Num(); i++)
				{
					if (slowBuffs[i]->isActivated == true)
					{
						strongestSlowBuff = slowBuffs[i];
					}
				}
				// COMPARE IT TO THE NEW BUFF
				if (newSlow->buffStrength > strongestSlowBuff->buffStrength)
				{
					// IF THE NEW BUFF IS STRONGER THEN, END THE BUFF OF THE PREVIOUS ONE
					strongestSlowBuff->EndBuff();
					strongestSlowBuff->isActivated = false;

					// THEN ACTIVATE THE NEW BUFF
					newSlow->isActivated = true;
					newSlow->StartBuff();

					// OTHERWISE SIMPLY DO NOTHING. 
				}
			}
			// ADD THE NEW BUFF TO THE LIST OF EXISTING BUFFS OF THAT TYPE REGARDLESS OF IT BEING ACTIVATED OR NOT 
			slowBuffs.Add(newSlow);
		}

		// IF ITS AN OVERLOAD BUFF
		else if (AOverloadBuff* newOverload = Cast<AOverloadBuff>(spawnedBuff))
		{
			if (overloadBuffs.Num() == NULL || overloadBuffs.Num() == 0)
			{
				// IF THERE ARE NO EXISTING BUFFS THEN ACTIVATE 
				newOverload->isActivated = true;
				newOverload->StartBuff();
			}
			else
			{
				AOverloadBuff* strongestOverloadBuff = overloadBuffs[0];
				// IF THE # OF THE LIST ISN'T ZERO. THEN THAT MEANS THERE IS AN ACTIVATAED BUFF OF THIS TYPE. GET THE CURRENT ACTIVATED STRONGEST BUFF
				// WHAT'S HELPING US HERE IS THAT THERE CAN ONLY BE ONE ACTIVE BUFF AT A TIME. 
				for (int i = 0; i < overloadBuffs.Num(); i++)
				{
					if (overloadBuffs[i]->isActivated == true)
					{
						strongestOverloadBuff = overloadBuffs[i];
					}
				}
				// COMPARE IT TO THE NEW BUFF
				if (newOverload->buffStrength > strongestOverloadBuff->buffStrength)
				{
					// IF THE NEW BUFF IS STRONGER THEN, END THE BUFF OF THE PREVIOUS ONE
					strongestOverloadBuff->EndBuff();
					strongestOverloadBuff->isActivated = false;

					// THEN ACTIVATE THE NEW BUFF
					newOverload->isActivated = true;
					newOverload->StartBuff();

					// OTHERWISE SIMPLY DO NOTHING. 
				}
			}
			// ADD THE NEW BUFF TO THE LIST OF EXISTING BUFFS OF THAT TYPE REGARDLESS OF IT BEING ACTIVATED OR NOT 
			overloadBuffs.Add(newOverload);
		}
		// end buff part also resolved using the isActivated variable. the buff won't "reset" the stats if isActivated is false. 

		spawnedBuff->OnBuffCalledToDestroy.AddDynamic(this, &UBuffManagerComponent::OnBuffDestroyed);

		// STILL ADD THE BUFF TO THE LIST OF ALL BUFFS
		listOfBuffs.Add(spawnedBuff);

		return spawnedBuff;
	}
	else
	{
		return NULL;
	}
}

void UBuffManagerComponent::SpawnTickBuff(TSubclassOf<class ABuff> buff, float nBuffStrength, float buffDurationMulti)
{
	// Spawn stuff
	FActorSpawnParameters spawnParams;
	spawnParams.SpawnCollisionHandlingOverride = ESpawnActorCollisionHandlingMethod::AlwaysSpawn;
	spawnParams.bNoFail = true;
	// spawnParams.Owner = this;

	FTransform spawnTransform;

	if (buff != NULL)
	{
		ABuff* spawnedBuff = GetWorld()->SpawnActor<ABuff>(buff, spawnTransform, spawnParams);

		// whenever a buff manager component spawns a buff, it will add it to its buff list 
		spawnedBuff->target = this->GetOwner();
		spawnedBuff->buffStrength = nBuffStrength;
		spawnedBuff->buffDuration *= buffDurationMulti;
		spawnedBuff->OnBuffCalledToDestroy.AddDynamic(this, &UBuffManagerComponent::OnBuffDestroyed);
		spawnedBuff->StartBuff(); // start the buff once target has been given 
		listOfBuffs.Add(spawnedBuff);
	}
}

void UBuffManagerComponent::EndBuffs()
{
	if (listOfBuffs.Num() != NULL)
	{
		for (int i = 0; i < listOfBuffs.Num(); i++)
		{
			// End it
			listOfBuffs[i]->EndBuff();
		}
	}
}

void UBuffManagerComponent::StartBuffs()
{
	if (listOfBuffs.Num() != NULL)
	{
		for (int i = 0; i < listOfBuffs.Num(); i++)
		{
			listOfBuffs[i]->StartBuff();
		}
	}
}

void UBuffManagerComponent::OnBuffDestroyed(ABuff* buff)
{
	// remove said buff from list 
	if (listOfBuffs.Contains(buff))
	{
		listOfBuffs.Remove(buff);
	}
}


