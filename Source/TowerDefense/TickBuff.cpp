// Fill out your copyright notice in the Description page of Project Settings.


#include "TickBuff.h"

// Called when the game starts or when spawned
void ATickBuff::BeginPlay()
{
	Super::BeginPlay();

	// activate the timer to do the effect 
	GetWorldTimerManager().SetTimer(DelayBetweenTicks, this, &ATickBuff::DoTickEffect, tickRate, true); // looping
}

void ATickBuff::DoTickEffect()
{
	// to be overriden 
}

void ATickBuff::ExecuteBehavior(float DeltaTime)
{

}
