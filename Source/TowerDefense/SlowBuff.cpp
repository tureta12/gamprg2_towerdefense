// Fill out your copyright notice in the Description page of Project Settings.


#include "SlowBuff.h"
#include "Enemy.h"
#include "GameFramework/CharacterMovementComponent.h"

void ASlowBuff::StartBuff()
{
	Super::StartBuff();
	float enemySpeed = Cast<AEnemy>(target)->baseSpeed;
	float modifiedSpeed = enemySpeed / buffStrength;

	Cast<AEnemy>(target)->ModifySpeed(modifiedSpeed);
}

void ASlowBuff::EndBuff()
{
	if (isActivated == true)
	{
		Super::EndBuff();
		float enemySpeed = Cast<AEnemy>(target)->speed; // speed value is the modified one already 
		float modifiedSpeed = enemySpeed * buffStrength;

		Cast<AEnemy>(target)->ModifySpeed(modifiedSpeed);
	}
}