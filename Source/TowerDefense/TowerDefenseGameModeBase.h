// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
//#include "WaveData.h"
#include "TowerDefenseGameModeBase.generated.h"

/**
 * 
 */
UCLASS()
class TOWERDEFENSE_API ATowerDefenseGameModeBase : public AGameModeBase
{
	GENERATED_BODY()
	
public: 
	ATowerDefenseGameModeBase();

protected:
	virtual void BeginPlay() override;

	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = "Player")
		class APlayer_Object* thePlayer;

public:
	UFUNCTION(BlueprintPure, BlueprintCallable)
		int32 GetMaxWaves();

	UFUNCTION(BlueprintPure, BlueprintCallable)
		int32 GetCurrentWaveNumber();

	UFUNCTION(BlueprintPure, BlueprintCallable)
		int32 GetTotalNumberEnemiesInWave();

	UFUNCTION(BlueprintPure, BlueprintCallable)
		int32 GetTotalDestroyedEnemiesInWave();

	UFUNCTION(BlueprintImplementableEvent)
		void GiveBuildManagerRefToUI(); // use this in conjunction with ReturnBuildManagerRef to do stuff in BP 

	UFUNCTION(BlueprintImplementableEvent)
		void GiveTowerReferenceToTowerMenu();

	UFUNCTION(BlueprintImplementableEvent)
		void OnPlayerDeath();

	UFUNCTION(BlueprintImplementableEvent)
		void OnGameWin();

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Setup")
		TArray<UWaveData*> WaveDatas;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		class ATower* latestClickedTower;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Stats")
		int32 waveNumber;

	//UFUNCTION(BlueprintImplementableEvent)
	//	void GiveTowerUIReference(class ATower* theTower);

private:
	UPROPERTY()
		FTimerHandle WaveDelayTimerHandle; 

	UPROPERTY(VisibleAnywhere, Category = "Stats")
		int32 totalNumberEnemiesInWave;

	UPROPERTY(VisibleAnywhere, Category = "Stats")
		int32 totalDestroyedEnemiesInWave; 

	UPROPERTY(VisibleAnywhere, Category = "Setup")
		TArray<AActor*> Spawners;

	UPROPERTY(VisibleAnywhere, Category = "Setup")
		TArray<AActor*> Cores;

	UPROPERTY(VisibleAnywhere)
		TArray<AActor*> BuildManagerRefs;

	UFUNCTION()
		void GiveSpawnersWaveData(UWaveData* waveData);

	UFUNCTION()
		void OnWaveEnd();

	UFUNCTION()
		void OnReadyForNextSubwaveSpawnerUpdate();

	UFUNCTION()
		void OnEnemySpawned(AEnemy* enemy);

	UFUNCTION()
		void OnEnemyReachedCore(int32 damage);

	UFUNCTION()
		void OnEnemyDeath(int32 reward);

	UFUNCTION(BlueprintCallable)
		class ABuildManager* ReturnBuildManagerRef();

	UFUNCTION()
		void OnBuildManagerSpawnedTower(class ATower* tower);

	UFUNCTION()
		void OnTowerClicked(class ATower* tower);
public:



};
