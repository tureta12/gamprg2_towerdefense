// Fill out your copyright notice in the Description page of Project Settings.


#include "StaticBuff.h"

AStaticBuff::AStaticBuff()
{
	isActivated = false;
}

void AStaticBuff::BeginPlay()
{
	Super::BeginPlay();
}

void AStaticBuff::StartBuff()
{
	Super::StartBuff();
}

void AStaticBuff::EndBuff()
{
	Super::EndBuff();
}