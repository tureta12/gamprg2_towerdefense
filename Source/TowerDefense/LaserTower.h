// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "SingleTargetTowers.h"
#include "LaserTower.generated.h"

/**
 * 
 */
UCLASS()
class TOWERDEFENSE_API ALaserTower : public ASingleTargetTowers
{
	GENERATED_BODY()
	
private:
	UPROPERTY(EditAnywhere)
		TSubclassOf<class ABuff>  BurnBuff;

	UPROPERTY(EditAnywhere, Category = "Stats")
		TArray<float> BuffDurationMulti;

	virtual void FindTarget() override; 

	virtual void Fire() override;

	virtual void OnUpgrade() override;
};
