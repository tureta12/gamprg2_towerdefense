// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Tower.h"
#include "ScavengerTower.generated.h"

/**
 * 
 */
UCLASS()
class TOWERDEFENSE_API AScavengerTower : public ATower
{
	GENERATED_BODY()
	
private:
	UPROPERTY(EditAnywhere, Category = "Stats")
		TArray<int32> passiveGold;

	UPROPERTY(EditAnywhere, Category = "Stats")
		TArray<float> extraGoldRate; // from kills

	UFUNCTION()
		void OnEnemyDeath(int32 enemyKillReward);

	virtual void BeginPlay() override;

	virtual void Fire() override;

	virtual void OnUpgrade() override;

	virtual void OnActorEnterOverlap(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp,
			int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult);

	virtual void OnActorExitOverlap(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp,
			int32 OtherBodyIndex);

};
