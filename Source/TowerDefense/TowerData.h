// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Engine/DataAsset.h"
#include "TowerData.generated.h"

/**
 * 
 */
UCLASS(BlueprintType)
class TOWERDEFENSE_API UTowerData : public UDataAsset
{
	GENERATED_BODY()
	
public:
	UPROPERTY(EditAnywhere)
		TSubclassOf<class ATower> Tower;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		FString towerName;

	//UPROPERTY(EditAnywhere, BlueprintReadWrite)
	//	class UTexture2D* image;
	
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		int32 goldCost;

};
