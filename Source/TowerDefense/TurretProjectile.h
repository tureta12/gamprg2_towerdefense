// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Projectile.h"
#include "TurretProjectile.generated.h"

/**
 * 
 */
UCLASS()
class TOWERDEFENSE_API ATurretProjectile : public AProjectile
{
	GENERATED_BODY()
	
private:
	void OnProjectileOverlapEffect(AActor* otherActor) override;
};
